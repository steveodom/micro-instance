import json
import boto3
from botocore.exceptions import ClientError

from technical.Persist import Persist
from utils.PersistUtils import sanitize_all

from utils.S3 import write as write_to_s3
from utils.Logger import Logger

class PersistCounts(object):
    def __init__(self):
        self.batch_items = {}

    def add_to_batch(self, id, key, direction, data):
        #data = sanitize_all(data)
        data['id'] = id
        data['direction'] = direction
        self.batch_items[key] = data
          
    def save_as_batch(self):
        """
        Batch write items to given table name
        """
        logger = Logger('technicalcounter').logger
        logger.info('saving batch...')

        try:
            write_to_s3('technicalcounter/daily.json', json.dumps(self.batch_items))
        except ClientError as e:
            logger.error("Unexpected error: %s" % e)
        
        return True