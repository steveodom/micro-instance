import boto3

class Query(object):
    def __init__(self, region_name="us-east-1", table_name=""):
        self.conn = boto3.resource('dynamodb', region_name=region_name)
        if table_name:
          self.table_name = table_name
          self.table = self.conn.Table(table_name)
    
    def count_items(self, key_expression, filter_expression = False):
        #sample filter_expression = Key('LOG_close_k_p9').eq('up')
        if filter_expression:
          response = self.table.query(
            FilterExpression=filter_expression,
            KeyConditionExpression=key_expression,
            Select='COUNT'
          )
        else:
          response = self.table.query(
            KeyConditionExpression=key_expression,
            Select='COUNT'
          )
        return response['Count']

    def scan(self, filter_expression, projection_expression):
        response = self.table.scan(
          FilterExpression=filter_expression,
          ProjectionExpression=projection_expression
        )
        return response
    
    def paginated_scan(self, filter_expression, projection_expression):
        response = self.table.scan(
          FilterExpression=filter_expression,
          ProjectionExpression=projection_expression
        )
        data = response['Items']
      
        while 'LastEvaluatedKey' in response:
          response = self.table.scan(
              ProjectionExpression=projection_expression,
              FilterExpression=filter_expression,
              ExclusiveStartKey=response['LastEvaluatedKey']
          )
          data.extend(response['Items'])
        return data
    
    def paginated_all(self):
        response = self.table.scan()
        data = response['Items']
      
        while 'LastEvaluatedKey' in response:
          response = self.table.scan()
          data.extend(response['Items'])
        return data
    