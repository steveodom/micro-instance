from utils.Dynamo import Dynamo

from .Builder import Builder
from .NameUtil import NameUtil
from utils.Logger import Logger

def run(period = "daily"):
  
  logger = Logger('technicalcounter').logger
  logger.info('running technical counter')
  # read/write capacity of History Table should already be enough since
  # this is likely be run after technical runner.
  tableHistory = "TechnicalIndicatorsCountsTable"
  Dynamo("us-east-1", tableHistory).update_throughput(5,50, wait = False)
  logger.info('updated throughput')
  items = Builder(period).query_and_save()
  
  logger.info('finished counting and aggregating')

  # front end helpers.
  NameUtil.save_all_names_to_s3()
  NameUtil.save_index_concordance(items)
  
  tableHistory = "TechnicalIndicatorsCountsTable"
  Dynamo("us-east-1", "TechnicalInstancesTable").update_throughput(5,5, wait = False)
  Dynamo("us-east-1", tableHistory).update_throughput(5,5, wait = False)
  Dynamo("us-east-1", "TechnicalInstancesTable").update_secondary_throughput('date_human-index', 5, 5)
  logger.info('lowered throughputs')
  logger.info('finished')