from boto3.dynamodb.conditions import Key, Attr
from utils.QueryUtil import QueryUtil as DynamoUtil
class QueryUtil(object):
    
    @staticmethod
    def object_builder(label, oid, klass, val = 1):
      s = label
      klass = klass or s
      dynamo = DynamoUtil.eq(klass, val)
      return {'s': s, 'd': dynamo, 'c': s, 'id': oid}
