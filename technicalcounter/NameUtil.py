import json
import re

from .Query import Query
from utils.S3 import write as write_to_s3
from itertools import permutations

class NameUtil(object):
    # def __init__(self):
    #     pass

    @staticmethod
    def save_all_names_to_s3():
        print('saving all names to s3...')
        names = Query("us-east-1", "IndicatorNamesTable").paginated_all()
        write_to_s3('technicalcounter/names.json', json.dumps(names))
        return names

    @staticmethod
    def save_recent_dates_to_s3(dates):
        print('saving all dates to s3...')
        write_to_s3('technicalcounter/recent_dates.json', json.dumps(dates))
        return dates

    @staticmethod
    def save_tickers_to_s3(tickers):
        print('saving all tickers to s3...')
        tickers = {"tickers": tickers}
        write_to_s3('technicalcounter/recent_dates.json', json.dumps(tickers))
        return tickers
      
    @staticmethod
    def save_index_concordance(items):
        print('saving index concordance...')
        keys = {}

        for key in items:
            key = re.sub(r'CH.*?-', '', key) #some combinations are run without the direction piece.
            ids = key.split("-")

            all = [list(x) for x in permutations(ids, len(ids))]
            for permutation in all:
                possible_search_key = "-".join(permutation)
                keys[possible_search_key] = key
        write_to_s3('technicalcounter/index.json', json.dumps(keys))
        return keys