from .Query import Query
from utils.QueryUtil import QueryUtil
from technical_ids.Change import Change
from technical_ids.Macd import Macd

from technical_ids.Patterns import Patterns
from technical_ids.Bollinger import Bollinger
from technical_ids.MovingAverage import MovingAverage
from technical_ids.Momentum import Momentum

from .Persist import PersistCounts

class Builder(object):
    def __init__(self, quote_type = 'daily'):
        self.quote_type = quote_type
        self.query_table = Query('us-east-1', 'TechnicalInstancesTable')
        self.persist_table = PersistCounts()
        self.period_query = QueryUtil.eq('period-index', quote_type)
        self.count_log = {}

    def save_individuals(self, queryables):
        for query in queryables:
            q = QueryUtil.eq('id', query['id']) 
            count = self.query_table.count_items(q)
            self.persist_table.add_to_batch(query['id'], query['id'], query['s'], {"count": count})
    
    def batch_two_queries(self, id, key, direction, filter_expression = False):
        query = QueryUtil.eq('id', id)
        count = self.query_table.count_items(query, filter_expression)
        self.persist_table.add_to_batch(id, key, direction, {"count": count})    

    def save_with_itself(self, queryables):
        for q1 in queryables:
            for q2 in queryables:
                id = "{}-{}".format(q2['id'], q1['id'])
                opp_id = "{}-{}".format(q1['id'], q2['id'])
                if q1['s'] is not q2['s'] and opp_id not in self.count_log:
                    self.count_log[id] = 1
                    self.batch_two_queries(id, id, 'all') 

    def save_with_change(self, queryables, change_queryables):
        for q1 in queryables:
            for q2 in change_queryables:
                id = q1['id']
                key= "{}-{}".format(q2['id'], q1['id'])
                self.count_log[key] = 1 #don't think we need this
                filter_expression = QueryUtil.eq("direction", q2['s'])
                self.batch_two_queries(id, key, q2['s'], filter_expression)
    
    def save_with_change_and_other(self, queryables, change_queryables, other_queryables):
        for q1 in queryables:
            for q2 in change_queryables:
                for q3 in other_queryables:
                    id = "{}-{}".format(q3['id'], q1['id'])
                    key = "{}-{}-{}".format(q2['id'], q3['id'], q1['id'])
                    opp_id = "{}-{}-{}".format(q2['id'], q1['id'], q3['id'])
                    if q1['s'] is not q3['s'] and opp_id not in self.count_log: 
                        filter_expression = QueryUtil.eq("direction", q2['s'])
                        self.batch_two_queries(id, key, q2['s'], filter_expression)
                        self.count_log[key] = 1
                    
    def query(self):
        change = Change()
        self.save_individuals(change.queryables())
        print('finished change')

        macd = Macd()
        self.save_individuals(macd.queryables())
        self.save_with_itself(macd.queryables())
        self.save_with_change(macd.queryables(), change.queryables())
        self.save_with_change_and_other(macd.queryables(), change.queryables(), macd.queryables())
        print('finished macd')
        
        patt = Patterns()
        self.save_individuals(patt.queryables())
        self.save_with_itself(patt.queryables())
        self.save_with_change(patt.queryables(), change.queryables())
        self.save_with_change_and_other(patt.queryables(), change.queryables(), patt.queryables())
        self.save_with_change_and_other(patt.queryables(), change.queryables(), macd.queryables())
        print('finished patterns')
                    
        boll = Bollinger()
        self.save_individuals(boll.queryables())
        self.save_with_itself(boll.queryables())
        self.save_with_change(boll.queryables(), change.queryables())
        self.save_with_change_and_other(boll.queryables(), change.queryables(), boll.queryables())
        self.save_with_change_and_other(boll.queryables(), change.queryables(), patt.queryables())
        self.save_with_change_and_other(boll.queryables(), change.queryables(), macd.queryables())
        print('finished bollinger')

        ma = MovingAverage()
        self.save_individuals(ma.queryables())
        self.save_with_itself(ma.queryables())
        self.save_with_change(ma.queryables(), change.queryables())
        self.save_with_change_and_other(ma.queryables(), change.queryables(), ma.queryables())
        self.save_with_change_and_other(ma.queryables(), change.queryables(), boll.queryables())
        self.save_with_change_and_other(ma.queryables(), change.queryables(), patt.queryables())
        self.save_with_change_and_other(ma.queryables(), change.queryables(), macd.queryables())
        print('finished moving average')

        mo = Momentum()
        
        self.save_individuals(mo.queryables())
        
        self.save_with_itself(mo.queryables())
        self.save_with_change(mo.queryables(), change.queryables())
        
        self.save_with_change_and_other(mo.queryables(), change.queryables(), mo.queryables())
        self.save_with_change_and_other(mo.queryables(), change.queryables(), boll.queryables())
        self.save_with_change_and_other(mo.queryables(), change.queryables(), patt.queryables())
        
        self.save_with_change_and_other(mo.queryables(), change.queryables(), macd.queryables())
        self.save_with_change_and_other(mo.queryables(), change.queryables(), ma.queryables())
        print('finished momentum')

        return self.persist_table.batch_items

    def add_opposite_counts(self):
        for id in self.persist_table.batch_items:
            row = self.persist_table.batch_items[id]
            opp_id = QueryUtil.find_opposite_id(id)
            
            if opp_id:
                opp_item = self.persist_table.batch_items[opp_id]
                row["opposite_count"] = opp_item["count"]

    def query_and_save(self):
        self.query()
        self.add_opposite_counts()
        print('about to save....')
        self.persist_table.save_as_batch()
        return self.persist_table.batch_items