from .CSV import CSV
from .Filenames import Filenames

class Query(object):
    def __init__(self, base_ticker, directory = "data"):
        self.base_ticker = base_ticker
        self.directory = directory
    

    def ticker_history(self, target_ticker, name, length):
        filename = Filenames(self.base_ticker, name, length).correlation()
        data = CSV(self.directory).open(filename)        
        return data[target_ticker]

    def tickers_comparison(self, target_tickers_array, name, length):
        filename = Filenames(self.base_ticker, name, length).correlation()
        data = CSV(self.directory).open(filename)        
        return data[target_tickers_array]

    def ticker_highest_corr(self, name, length):
        filename = Filenames(self.base_ticker, name, length).highs()
        data = CSV(self.directory).open(filename)        
        return data

    def ticker_group_history(self, name, length):
        filename = Filenames(self.base_ticker, name, length).group_membership()
        data = CSV(self.directory).open(filename)
        return data
    
    def group_correlations(self, name, length):
        filename = Filenames(self.base_ticker, name, length).group_correlations()
        data = CSV(self.directory).open(filename)        
        return data