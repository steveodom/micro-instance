
class Filenames(object):
    def __init__(self, ticker, name, length):
        self.ticker = ticker
        self.name = name
        self.length = length

    def base(self):
        return "{0}-{1}-{2}len".format(self.ticker, self.name, self.length)
    
    def correlation(self):
        return self.base()

    def highs(self):
        return "{0}-high-ticker".format(self.base())

    def group_membership(self):
        return "{0}-group-membership".format(self.base())

    def group_correlations(self):
        return "{0}-group-correlation".format(self.base())  