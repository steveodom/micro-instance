from Query import Query
import sys
import argparse


def run(flags):
  base_ticker = flags.base_ticker or "AAPL"
  target_ticker = flags.target_ticker or "FB"
  name = flags.period_offset or "5_periods"
  length = flags.window_length or "25"

  Query(base_ticker).ticker_history(target_ticker, name, length)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--base_ticker', type=str, default=None,
                        help='Example: --base_ticker=AAPL')

    parser.add_argument('--target_ticker', type=str, default=None,
                        help='Example: --target_ticker=FB')
    
    parser.add_argument('--period_offset', type=str, default=None,
                        help='Example: --period_offset=5_periods')
    
    parser.add_argument('--window_length', type=str, default=None,
                        help='Example: --window_length=25')

    FLAGS, unparsed = parser.parse_known_args()
    run(FLAGS)