import csv
import os
import pandas as pd
from pathlib import Path

class CSV(object):
    def __init__(self, directory = "data/"):
        self.directory = directory
    
    def open(self, filename):
        name = "{0}/{1}.csv".format(self.directory, filename)
        key = Path(os.getcwd(), name)
        #print(key)
        if os.path.isfile(key):
          return pd.read_csv(key)
        else:
          return None
  