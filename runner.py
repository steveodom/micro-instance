import sys
import argparse

from correlations.runner import run as correlations_runner
from feature_builder.runner import run as features_runner
from technicalcounter.runner import run as technicalcounter_runner
from technical.runner import run as technical_runner
from utils.Tester import run as tester_runner
from utils.Logger import Logger

from ec2 import stop_instance

from technical.config import (
    PERIODS_TO_CALC,
    NUMBER_OF_QUOTE_PERIODS
)


def run(should_run, should_stop_instance=False, flags={}):
    logger = Logger('general-runner').logger
    logger.info('starting')
    should_run = flags.should_run or "correlations"
    trigger_source = flags.trigger_source or "undefined"
    
    if should_run == 'correlations':
        print('running correlations')
        correlations_runner(trigger_source)
    
    if should_run == 'features':
        print('running features')
        features_runner('MSFT', 6)

    if should_run == 'technicalcounter':
        period = flags.technical_period or "daily"
        technicalcounter_runner(period)
    
    if should_run == 'technical':
        logger.info('running technical')
        period = flags.technical_period or "daily"
        do_auxillary_steps = flags.technical_do_auxillary_steps
        periods_to_calc = flags.technical_periods_to_calc
        number_of_quotes = flags.technical_quote_number
        technical_runner(period, do_auxillary_steps, periods_to_calc, number_of_quotes)
    
    if should_run == 'tester':
        logger.info('running tester')
        tester_runner()

    if should_stop_instance:
        print('stopping instance')
        stop_instance('i-08e0a02cbbdd423f1')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--should_run', type=str, default=None,
                        help='Example: --should_run=correlations')

    parser.add_argument('--stop_instance', dest='should_stop_instance', action='store_true',
                      help='Example: --stop_instance')
    
    parser.add_argument('--dont_stop_instance', dest='should_stop_instance', action='store_false',
                      help='Example: --stop_instance')
    
    parser.add_argument('--trigger_source', type=str, default="unspecified",
                      help='Example: --trigger_source=nightly-rule')
    

    parser.add_argument('--technical_period', type=str, default="daily",
                      help='Example: --technical_period=daily')

    parser.add_argument('--technical_quote_number', type=int, default=NUMBER_OF_QUOTE_PERIODS,
                      help='Example: --technical_quote_number=200')
    
    parser.add_argument('--technical_periods_to_calc', type=int, default=PERIODS_TO_CALC,
                      help='Example: --technical_periods_to_calc=5')

    parser.add_argument('--technical_do_auxillary_steps', dest='technical_do_auxillary_steps', action='store_true',
                      help='Example: --technical_do_auxillary_steps')

    parser.add_argument('--technical_dont_do_auxillary_steps', dest='technical_do_auxillary_steps', action='store_false',
                      help='Example: --technical_dont_do_auxillary_steps')

    # parser.add_argument('--correlation_frame_size', type=int,
    #                   help='Example: --correlation_frame_size=50')

    FLAGS, unparsed = parser.parse_known_args()
    run(FLAGS.should_run, bool(FLAGS.should_stop_instance), FLAGS)