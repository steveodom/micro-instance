# To run:
python runner.py --should_run=correlations --should_stop_instance=False --trigger_source=manual 

# To log in to EC2:
ssh -v -i pytorch.pem ubuntu@34.233.121.35

The runner.py file should be run automatically on instance startup.

The instance can be started on a schedule with instance-starter lambda.

To execute the runner file on startup, the instance user data is edited to include 'cd micro-instance python3 runner.py'

To edit the instance user data from the web console, right-click the instance and select Instance Settings > View/Change User Data.

# user data script:
#cloud-boothook
#!/bin/bash
/bin/echo "Hello World." >> /tmp/steve
cd /home/ubuntu/micro-instance/
python3 runner.py --should_run=correlations --should_stop_instance=True --trigger_source=nightly-rule

The #cloud-boothook needs to be there (a hack) for the user-script to run everytime.


# TODO:
1. integrate AWS code deploy so the micro-instance work can easily be updated.

To update code now, you must ssh into the instance and pull down the most recent changes.
