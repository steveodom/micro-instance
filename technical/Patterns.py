from talib.abstract import (
    CDL2CROWS, 
    CDL3BLACKCROWS, 
    CDLDOJI,
    CDLDOJISTAR,
    CDLDRAGONFLYDOJI,
    CDLENGULFING,
    CDLEVENINGDOJISTAR,
    CDLEVENINGSTAR
)

from technical_ids.Patterns import Patterns as Id

class Patterns(object):
    
    def __init__(self, df):
        self.df = df
    
    def add(self, inputs):
        self.df.loc[:,Id().twoCrows()['id']] = CDL2CROWS(inputs)
        self.df.loc[:,Id().threeCrows()['id']] = CDL3BLACKCROWS(inputs)
        self.df.loc[:,Id().doji()['id']] = CDLDOJI(inputs)
        self.df.loc[:,Id().dojiStar()['id']] =  CDLDOJISTAR(inputs)
        self.df.loc[:,Id().engulfing()['id']] = CDLENGULFING(inputs)
        self.df.loc[:,Id().dragonflyDoji()['id']] = CDLDRAGONFLYDOJI(inputs)
        
        self.df.loc[:,Id().eveningDojiStar()['id']] = CDLEVENINGDOJISTAR(inputs)
        self.df.loc[:,Id().eveningStar()['id']] = CDLEVENINGSTAR(inputs)