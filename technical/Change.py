import numpy as np
from scipy.stats import zscore, linregress
from technical_ids.Change import Change as Id

class Change(object):
    def __init__(self, df):
        self.df = df
        self.change_std_threshold = 0.5

    def zscore(self, period = 1):
        if period > 0:
            is_future = False
        else:
            is_future = True

        log_id = Id().log(period, is_future)
        z_id = Id().zscore(period, is_future)
        self.df[log_id] = round(np.log(self.df['close']) - np.log(self.df['close'].shift(period)),4)
        self.df[log_id].fillna(0, inplace=True)
        self.df[z_id] = self.df[[log_id]].apply(zscore)
        self.df[z_id].fillna(0, inplace=True)
    
    # TODO: add options to check for max or min.
    def classify_growth(self, row, label):
      val = row[label]
      if val >= self.change_std_threshold:
        return 'up'
      elif val <= -1 * self.change_std_threshold:
        return 'down'
      else: 
        if val == 0.0:
            return 'na'
        else:
            return 'flat'

    def future(self, period=-1):
        if period > 0:
            is_future = False
        else:
            is_future = True
        date_id = Id().target_date(period, is_future)
        z_id = Id().zscore(period, is_future)
        
        self.zscore(period)
        self.df[date_id] = self.df['date'].shift(period)
        self.df.loc[:,'direction'] = self.df.apply(lambda row: self.classify_growth(row, z_id), axis=1)