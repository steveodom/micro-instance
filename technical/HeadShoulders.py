import numpy as np
import peakutils

from technical_ids.Patterns import Patterns as Id

class HeadShoulders(object):
    # I modified the rules slightly from this page:
    # https://quant.stackexchange.com/questions/1937/how-to-identify-technical-analysis-chart-patterns-algorithmically
    # Mainly, I lowered the % each shoulder piece needs to be within
    # to 1% rather than 1.5%    
    def __init__(self, df):
        self.df = df
        self.indexes_high = None
        self.indexes_low = None
        self.matched_verted_series = []
        self.matched_inverted_series = []
        self.matched_verted_dates = []
        self.matched_inverted_dates = []
    
    def find_next_extrema(self, ei, klass="high"):
        if klass == "high":
            index = self.indexes_high
        else:
            index = self.indexes_low
        
        future = [i for i in index if i > ei]
        if future:
            i = future[0]
            e = self.df.iloc[i]
            return i, e
        else:
            return False, False

    def avg_range_needed(self, i, ii):
        avg = (i + ii) / 2
        min_avg = avg * .99
        max_avg = avg * 1.01
        return min_avg, max_avg

    def points_are_close(self, i, ii):
        min_avg, max_avg = self.avg_range_needed(i, ii)
        return min_avg < i < max_avg and min_avg < ii < max_avg
    
    def find_extrema(self):
        self.indexes_high = peakutils.indexes(self.df['close'], thres=0.1, min_dist=2)
        self.indexes_low  = peakutils.indexes(-self.df['close'], thres=0.1, min_dist=2)
    
    def padded_greater_than(self, price, padded_price, threshold="small"):
        if threshold is not "small":
          threshold = 1.004
        else:
          threshold = 1.001
        return price > padded_price * threshold
    
    def is_too_spreadout_timewise(self, first_index, last_index):
        return last_index - first_index > 18
    
    def find_verted(self):
        for e1i in self.indexes_high:
            extrema = [0,0,0,0,0]
            e1 = self.df.iloc[e1i]
            e2, e2i = None, None
            
            e2i, e2 = self.find_next_extrema(e1i, "low")
            if not e2i:
                continue
            
            if self.padded_greater_than(e1['close'],e2['close']):
                extrema[0] = e1i
            else:
                continue
            
            e3i, e3 = self.find_next_extrema(e2i, "high")
            if not e3i:
                continue
                
            if self.padded_greater_than(e3['close'], e1['close'], "large"):
                extrema[1] = e2i
                extrema[2] = e3i
            else:
                continue
    
            e4i, e4 = self.find_next_extrema(e3i, "low")
            if not e4i:
                continue
                    
            if self.points_are_close(e2['close'], e4['close']):
                extrema[3] = e4i
            else:
                continue
                
            e5i, e5 = self.find_next_extrema(e4i, "high")
            if not e5i:
                continue
            
            if self.points_are_close(e1['close'], e5['close']):
                extrema[4] = e5i
            else:
                continue
            
            # the last peak (e5) must be greater than the e4 bottom
            if not self.padded_greater_than(e5['close'], e4['close']):
                continue

            if self.is_too_spreadout_timewise(e1i, e5i):
                continue

            # middle peak (e3) is higher than e5 peaks
            if self.padded_greater_than(e3['close'], e5['close'], "large"):
                series = self.df.iloc[extrema]['close']
                self.matched_verted_series.append(series)

    def find_inverted(self):
        for e1i in self.indexes_low:
            extrema = [0,0,0,0,0]
            e1 = self.df.iloc[e1i]
            e2, e2i = None, None
            
            e2i, e2 = self.find_next_extrema(e1i, "high")
            if not e2i:
                continue
            
            if self.padded_greater_than(e2['close'], e1['close']):
                extrema[0] = e1i
            else:
                continue
            
            e3i, e3 = self.find_next_extrema(e2i, "low")
            if not e3i:
                continue
                
            if self.padded_greater_than(e1['close'], e3['close'], "large"):
                extrema[1] = e2i
                extrema[2] = e3i
            else:
                continue
            
            e4i, e4 = self.find_next_extrema(e3i, "high")
            if not e4i:
                continue
                    
            if self.points_are_close(e2['close'], e4['close']):
                extrema[3] = e4i
            else:
                continue
                
            e5i, e5 = self.find_next_extrema(e4i, "low")
            if not e5i:
                continue
            
            if self.points_are_close(e1['close'], e5['close']):
                extrema[4] = e5i
            else:
                continue
            
            # the last peak (e5) must be less than the e4 bottom
            if not self.padded_greater_than(e4['close'], e5['close']):
                continue
            
            if self.is_too_spreadout_timewise(e1i, e5i):
                continue
                    
            # middle peak (e3) is low than e5 peaks
            if self.padded_greater_than(e5['close'], e3['close']):
                series = self.df.iloc[extrema]['close']
                self.matched_inverted_series.append(series)
    
    def extract_last_dates_of_matches(self):
        for s in self.matched_verted_series:
            self.matched_verted_dates.append(s.index[-1])
        
        for s in self.matched_inverted_series:
            self.matched_inverted_dates.append(s.index[-1])

    def add(self):
        self.find_extrema()
        self.find_verted()
        self.find_inverted()
        self.extract_last_dates_of_matches()

        hs_id = Id().headAndShoulders()['id']
        hsi_id = Id().headAndShouldersInverted()['id']
        self.df[hs_id] = np.where(self.df.index.isin(self.matched_verted_dates), 1, 0)
        self.df[hsi_id] = np.where(self.df.index.isin(self.matched_inverted_dates), 1, 0)