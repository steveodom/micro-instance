from talib.abstract import EMA

from technical_ids.MovingAverage import MovingAverage as Id

class MovingAverage(object):
    def __init__(self, df):
        self.df = df

    def ma(self, df, period = 5):
        return df.rolling(period).mean()
        
    def add(self):
        base_five_id = Id().base(5)['id']
        base_two_id = Id().base(200)['id']

        self.df.loc[:,base_five_id] = self.ma(self.df['close'], 5)
        self.df.loc[:,base_two_id] = self.ma(self.df['close'], 200)
       
        # upper breaches
        five_id = Id().upper_breach_5d_close()['id']
        two_id = Id().upper_breach_200d_close()['id']

        self.df.loc[:, five_id] = self.df.apply(lambda row: self.above_upper_breach(row, 'close', base_five_id), axis=1)
        self.df.loc[:, two_id] = self.df.apply(lambda row: self.above_upper_breach(row, 'close', base_two_id), axis=1)

        # lower
        five_id = Id().lower_breach_5d_close()['id']
        two_id = Id().lower_breach_200d_close()['id']
        self.df.loc[:, five_id] = self.df.apply(lambda row: self.below_lower_breach(row, 'close', base_five_id), axis=1)
        self.df.loc[:, two_id] = self.df.apply(lambda row: self.below_lower_breach(row, 'close', base_two_id), axis=1)

    def above_upper_breach(self, row, col, threshold_col):
        if row[col] > row[threshold_col]: 
          return 1
        else:
          return 0
    
    def below_lower_breach(self, row, col, threshold_col):
        if row[col] < row[threshold_col]: 
          return 1
        else:
          return 0


