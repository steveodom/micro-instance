from .RawData import RawData

from technical_ids.Change import Change
from technical_ids.Macd import Macd
from technical_ids.Momentum import Momentum
from technical_ids.Patterns import Patterns
from technical_ids.Bollinger import Bollinger
from technical_ids.MovingAverage import MovingAverage

from technicalcharter.Macd import Macd as MacdChart
from technicalcharter.Price import Price as PriceChart

from .Persist import Persist
from utils.DateAndTime import string_date_to_number, time_now

from tickers.tickers import TICKER_DETAIL

class Builder(object):
    def __init__(self, ticker, df, lookahead_period = 5, lookback_period = 10, quote_type = 'daily'):
        self.ticker = ticker
        self.df = df
        self.lookahead_period = lookahead_period
        self.lookback_period = lookback_period
        self.quote_type = quote_type
        self.persist_table = Persist('us-east-1', 'TechnicalInstancesTable')
        self.count_log = {}
        self.base_data = {}

    def save_individuals(self, row, queryables):
        for query in queryables:
            id = query['id']
            if row[id] == 1:
              data = self.base_data.copy()
              data['id'] = id
              data[id] = row[id]
              if row['direction'] is not "flat":
                data['direction'] = row['direction']
              else:
                data['needs_updating'] = True
              self.persist_table.add_to_batch(id, data)
    
    def batch_two_queries(self, row, id, q1, q2):
        q1_id = q1['id']
        q2_id = q2['id']
        if row[q1_id] == 1 and row[q2_id] == 1:
          data = self.base_data.copy()
          data['id'] = id
          data[q1_id] = row[q1_id]
          data[q2_id] = row[q2_id]
          if row['direction'] is not "flat":
            data['direction'] = row['direction']
          else:
            data['needs_updating'] = True
          self.persist_table.add_to_batch(id, data)
          self.count_log[id] = data

    def save_with_other(self, row, queryables, other_queryables):
        for q1 in queryables:
            for q2 in other_queryables:
                id = "{}-{}".format(q2['id'], q1['id'])
                opp_id = "{}-{}".format(q1['id'], q2['id'])
                if q1['s'] is not q2['s'] and opp_id not in self.count_log:
                    self.batch_two_queries(row, id, q1, q2) 
    
    def save_with_change(self, row, queryables, change_queryables):
        for q1 in queryables:
            for q2 in change_queryables:
                id = "{}-{}".format(q2['id'], q1['id'])
                self.batch_two_queries(row, id, q1, q2)
    
    def set_base_data(self, row):
        if self.lookahead_period > 0:
            is_future = False
        else:
            is_future = True
        future_log_id = Change().log(self.lookahead_period, is_future)
        future_date_id = Change().target_date(self.lookahead_period, is_future)
        if future_log_id in row:
            future_growth = row[future_log_id]
        else:
            future_growth = 0
        
        if future_date_id in row:
            future_date = row[future_date_id]
        else:
            future_date = "na"
        
        cap = "S&P500"
        if self.ticker in TICKER_DETAIL:
            cap = TICKER_DETAIL[self.ticker]["size"] 
        
        self.base_data = {
          "date_number": string_date_to_number(row['date']),
          "date_human": row['date'],
          "period": self.quote_type,
          "ticker": self.ticker,
          "cap": cap,
          "lookahead_period": self.lookahead_period,
          "lookback_period": self.lookback_period,
          "future_growth": future_growth,
          "future_target_date": future_date,
          "created_at": time_now()
        }
    
    def query(self, row):
        self.set_base_data(row)
        
        macd = Macd()
        self.save_individuals(row, macd.queryables())
        self.save_with_other(row, macd.queryables(), macd.queryables())
        print('finished macd')
        
        patt = Patterns()
        self.save_individuals(row, patt.queryables())
        self.save_with_other(row, patt.queryables(), patt.queryables())
        self.save_with_other(row, patt.queryables(), macd.queryables())
        print('finished patterns')
                    
        boll = Bollinger()
        self.save_individuals(row, boll.queryables())
        self.save_with_other(row, boll.queryables(), boll.queryables())
        self.save_with_other(row, boll.queryables(), patt.queryables())
        self.save_with_other(row, boll.queryables(), macd.queryables())
        print('finished bollinger')

        ma = MovingAverage()
        self.save_individuals(row, ma.queryables())
        self.save_with_other(row, ma.queryables(), ma.queryables())
        self.save_with_other(row, ma.queryables(), boll.queryables())
        self.save_with_other(row, ma.queryables(), patt.queryables())
        self.save_with_other(row, ma.queryables(), macd.queryables())

        mo = Momentum()
        self.save_individuals(row, mo.queryables())
        self.save_with_other(row, mo.queryables(), mo.queryables())
        self.save_with_other(row, mo.queryables(), boll.queryables())
        self.save_with_other(row, mo.queryables(), patt.queryables())
        self.save_with_other(row, mo.queryables(), macd.queryables())
        self.save_with_other(row, mo.queryables(), ma.queryables())

        print('finished moving average', self.ticker)
        return self.persist_table.batch_items

    def query_and_save(self, row):
        if self.df is not None:
          self.query(row)
          return self.persist_table.batch_items