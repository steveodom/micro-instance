from datetime import datetime 

from quotes.Dataframe import Dataframe
from .Change import Change
from .Macd import MACD
from .Momentum import Momentum
from .Patterns import Patterns
from .HeadShoulders import HeadShoulders
from .Bollinger import Bollinger
from .MovingAverage import MovingAverage

class RawData(object):
    def __init__(self, quote_type, lookahead_period = -5, lookback_period = 10, number_of_quotes = 10000):
        self.df = None
        self.quote_type = quote_type
        self.lookahead_period = lookahead_period
        self.lookback_period = lookback_period
        self.number_of_quotes = number_of_quotes

    def get_quote(self, ticker):
        self.df = Dataframe(self.quote_type, ticker, self.number_of_quotes).build()
        
    def insert_id(self):
        self.df.insert(0, 'id', range(0, len(self.df)))
    
    def to_array(self):
        self.as_array = {
            'open': self.df['open'].values,
            'high': self.df['high'].values,
            'low': self.df['low'].values,
            'close': self.df['close'].values,
            'volume': self.df['volume'].astype('float64').values
        }
        
    def format(self, ticker):
        self.get_quote(ticker)
        if self.df is not None:
            self.insert_id()
            Change(self.df).zscore(self.lookback_period)
            self.to_array()
            MovingAverage(self.df).add()
            Bollinger(self.df).add(self.as_array)
            Change(self.df).future(self.lookahead_period) 
            Patterns(self.df).add(self.as_array)
            MACD(self.df).add(self.as_array)
            Momentum(self.df).add(self.as_array)
            HeadShoulders(self.df).add()
            return self.df


        
