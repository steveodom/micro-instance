from talib.abstract import MACDFIX

from technical_ids.Macd import Macd as Id

class MACD(object):
    # Developed by Thomas Aspray in 1986, the MACD-Histogram measures the distance 
    # between MACD and its signal line (the 9-day EMA of MACD). 
    # Like MACD, the MACD-Histogram is also an oscillator that 
    # fluctuates above and below the zero line. Aspray developed 
    # the MACD-Histogram to anticipate signal line crossovers in 
    # MACD. Because MACD uses moving averages and moving averages
    # lag price, signal line crossovers can come late and affect the 
    # reward-to-risk ratio of a trade. Bullish or bearish divergences in the MACD-Histogram can alert chartists to an imminent signal line crossover in MACD. See our ChartSchool article for more on MACD. 
    def __init__(self, df):
        self.df = df
        
    def add(self, inputs):
      macd, macdsignal, macdhist = MACDFIX(inputs, signalperiod=9)
      #MACD Base
      base_id = Id().base()['id']
      self.df.loc[:,base_id] = macd

      #MACD Histogram
      histogram_id = Id().histogram()['id']
      self.df.loc[:,histogram_id] = macdhist

      #MACD Signal
      signal_id = Id().signal()['id']
      self.df.loc[:,signal_id] = macdsignal #EMA(self.as_array, timeperiod=9)
      
      #MACD Bull Signal Cross
      id = Id().bull_signal_x()['id']
      self.df.loc[:,id] = self.df.apply(lambda row: MACD.is_bullish_macd_crossover(row, self.df), axis=1)
      
      #MACD Bear Signal Cross
      id = Id().bear_signal_x()['id']
      self.df.loc[:,id] = self.df.apply(lambda row: MACD.is_bearish_macd_crossover(row, self.df), axis=1)

      #MACD Bull Center Cross
      id = Id().bull_center_x()['id']
      self.df.loc[:,id] = self.df.apply(lambda row: MACD.is_bullish_center_crossover(row, self.df), axis=1)
      
      #MACD Bear Center Cross
      id = Id().bear_center_x()['id']
      self.df.loc[:,id] = self.df.apply(lambda row: MACD.is_bearish_center_crossover(row, self.df), axis=1)
        
    @staticmethod
    def is_bullish_macd_crossover(row, df):
      id = row['id']
      prev = df.iloc[id-1,:]
      prev_2 = df.iloc[id-2,:]

      base_id = Id().base()['id']
      signal_id = Id().signal()['id']

      macd_was_less_than_signal_2_days_ago = prev[base_id] < prev_2[signal_id]
      macd_is_more_than_signal_1_day_ago = row[base_id] > prev[signal_id]
      if macd_was_less_than_signal_2_days_ago and macd_is_more_than_signal_1_day_ago:
        return 1
      else:
        return 0

    @staticmethod
    def is_bearish_macd_crossover(row, df):
      id = row['id']
      prev = df.iloc[id-1,:]
      prev_2 = df.iloc[id-2,:]

      base_id = Id().base()['id']
      signal_id = Id().signal()['id']

      macd_was_more_than_signal_2_days_ago = prev[base_id] > prev_2[signal_id]
      macd_is_less_than_signal_1_day_ago = row[base_id] < prev[signal_id]
      
      if macd_was_more_than_signal_2_days_ago and macd_is_less_than_signal_1_day_ago:
        return 1
      else:
        return 0
      

    @staticmethod
    def is_bullish_center_crossover(row, df):
      id = row['id']
      prev = df.iloc[id-1,:]
      prev_2 = df.iloc[id-2,:]

      base_id = Id().base()['id']
      
      macd_was_below_zero_yesterday = prev[base_id] < 0
      macd_is_more_than_zero_today = row[base_id] > 0
      if macd_was_below_zero_yesterday and macd_is_more_than_zero_today:
        return 1
      else:
        return 0
      
    
    @staticmethod
    def is_bearish_center_crossover(row, df):
      id = row['id']
      prev = df.iloc[id-1,:]
      prev_2 = df.iloc[id-2,:]

      base_id = Id().base()['id']

      macd_was_more_than_zero_yesterday = prev[base_id] > 0
      macd_is_less_than_zero_today = row[base_id] < 0
      if macd_was_more_than_zero_yesterday and macd_is_less_than_zero_today:
        return 1
      else:
        return 0
  