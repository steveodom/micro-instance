# Example runner:
python3 runner.py --should_run=technical --should_stop_instance=False --trigger_source=nightly-rule --technical_do_auxillary_steps=False --technical_periods_to_calc=1400 --technical_quote_number=1600

# Steps to add a new indicator

1. Setup the ids in technical_ids
  -- including adding the new references under "queryables"

2. Setup the calculation in technical under "add":

3. Add the calculation "add" reference to technical/RawData "format"

4. If it's a new category of calculations, then you must add it in technical/Builder

5. Add the appropriate references to the new calculations to dynamodb/IndicatorNamesTable