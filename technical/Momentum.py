from talib.abstract import MFI, BOP, CCI

from technical_ids.Momentum import Momentum as Id

class Momentum(object):
    def __init__(self, df):
        self.df = df
    
    def cci_overbought(self, row, col):
        if row[col] > 100: 
          return 1
        else:
          return 0
    
    def cci_oversold(self, row, col):
        if row[col] < -100: 
          return 1
        else:
          return 0

    def moneyflow_overbought(self, row, col):
        if row[col] > 80: 
          return 1
        else:
          return 0
    
    def moneyflow_oversold(self, row, col):
        if row[col] < 20: 
          return 1
        else:
          return 0
    
    def bop_positive(self, row, col):
        if row[col] > 0: 
          return 1
        else:
          return 0
    
    def bop_negative(self, row, col):
        if row[col] < 0: 
          return 1
        else:
          return 0

    def add(self, inputs):
      # MFI
      mfi = MFI(inputs, timeperiod=14)
      mfi_id = Id().moneyflow_base()['id']
      self.df.loc[:,mfi_id] = mfi

      mfi_overbought_id = Id().moneyflow_overbought()['id']
      mfi_oversold_id = Id().moneyflow_oversold()['id']
      
      self.df.loc[:, mfi_overbought_id] = self.df.apply(lambda row: self.moneyflow_overbought(row, mfi_id), axis=1)
      self.df.loc[:, mfi_oversold_id] = self.df.apply(lambda row: self.moneyflow_oversold(row, mfi_id), axis=1)

      bop = BOP(inputs, timeperiod=14)
      bop_id = Id().bop_base()['id']
      self.df.loc[:,bop_id] = bop

      bop_positive_id = Id().bop_positive()['id']
      bop_negative_id = Id().bop_negative()['id']
      
      self.df.loc[:, bop_positive_id] = self.df.apply(lambda row: self.bop_positive(row, bop_id), axis=1)
      self.df.loc[:, bop_negative_id] = self.df.apply(lambda row: self.bop_negative(row, bop_id), axis=1)

      cci = CCI(inputs, timeperiod=14)
      cci_id = Id().cci_base()['id']
      self.df.loc[:,cci_id] = cci

      cci_overbought_id = Id().cci_overbought()['id']
      cci_oversold_id = Id().cci_oversold()['id']
      
      self.df.loc[:, cci_overbought_id] = self.df.apply(lambda row: self.cci_overbought(row, cci_id), axis=1)
      self.df.loc[:, cci_oversold_id] = self.df.apply(lambda row: self.cci_oversold(row, cci_id), axis=1)