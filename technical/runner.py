from decimal import Decimal 
from math import isnan

from .Builder import Builder
from .RawData import RawData
from .config import (
    TICKERS, 
    PERIODS_TO_CALC,
    NUMBER_OF_QUOTE_PERIODS, 
    LOOKAHEAD_PERIODS, 
    LOOKBACK_PERIODS
)

from technicalcharter.Macd import Macd
from technicalcharter.Price import Price

from technicalcounter.runner import run as count_runner
from technicalcounter.NameUtil import NameUtil

from utils.Dynamo import Dynamo
from utils.Logger import Logger

def run(
        quote_period, 
        do_auxillary_steps = True, 
        periods_to_calc = PERIODS_TO_CALC,
        number_of_quotes = NUMBER_OF_QUOTE_PERIODS
    ):
    logger = Logger('technical').logger
    logger.info('running technical')

    if do_auxillary_steps:
        tableHistory = "TechnicalInstancesTable"
        Dynamo("us-east-1", tableHistory).update_throughput(5,100, wait = False)
        Dynamo("us-east-1", tableHistory).update_secondary_throughput('date_human-index', 5,100)
    
    frame_size = 45
    save_location = 's3'
    simple_only = True
    prefix = "indicators"
    dates = []
    
    for num, ticker in enumerate(TICKERS):
        print(ticker)
        df = RawData(
            quote_period, 
            LOOKAHEAD_PERIODS, 
            LOOKBACK_PERIODS, 
            number_of_quotes
        ).format(ticker)
        
        if df is None:
            continue

        builder = Builder(ticker, df, LOOKAHEAD_PERIODS, LOOKBACK_PERIODS, quote_period)
        macd = Macd(df, ticker, frame_size)
        price = Price(df, ticker, frame_size)
        
        d = df.to_dict('index')

        num_of_data_points = len(df)
        start_point_of_index_to_calculate = num_of_data_points - periods_to_calc
        
        for i, date in enumerate(sorted(d)):
            if num == 0:
                dates.append(date)
            row = d[date]
            row_id = row['id']
            
            if i < start_point_of_index_to_calculate:
                print('not doing anything')
                continue
            
            batch_items = builder.query_and_save(row)
            # if row_id > frame_size:
            #     macd.chart(row_id, save_location, simple_only, prefix)
            #     price.chart(row_id, save_location, simple_only, prefix)
        builder.persist_table.save_as_batch(ticker)
        # print(builder.persist_table.batch_items)

    logger.info('finished first part of technical')
    if do_auxillary_steps:
        logger.info('doing aux steps')
        tableHistory = "TechnicalInstancesTable"
        Dynamo("us-east-1", tableHistory).update_throughput(50,5, wait = False)
        Dynamo("us-east-1", tableHistory).update_secondary_throughput('date_human-index', 50, 5)
        
        NameUtil().save_tickers_to_s3(TICKERS)
        NameUtil().save_recent_dates_to_s3(dates)
        logger.info('initiating count runner')
        count_runner(quote_period)