from talib.abstract import BBANDS
from technical_ids.Bollinger import Bollinger as Id

class Bollinger(object):
    
    def __init__(self, df):
        self.df = df

    def upper_id(self):
      return Id().upper()['id']

    def lower_id(self):
      return Id().lower()['id']

    def average_id(self):
      return Id().average()['id']

    def upper_breach_close(self):
      return Id().upper_breach_close()['id']

    def lower_breach_close(self):
      return Id().lower_breach_close()['id']

    def upper_breach_high(self):
      return Id().upper_breach_high()['id']

    def lower_breach_low(self):
      return Id().lower_breach_low()['id']
    
    def add(self, inputs):
      upper, middle, lower = BBANDS(inputs, 20, 2, 2)
      self.df.loc[:,self.upper_id()], self.df.loc[:,self.average_id()], self.df.loc[:,self.lower_id()] = upper, middle, lower
      
      # close breaches
      self.df.loc[:, self.upper_breach_close()] = self.df.apply(lambda row: self.above_upper_breach(row, 'close'), axis=1)
      self.df.loc[:, self.lower_breach_close()] = self.df.apply(lambda row: self.below_lower_breach(row, 'close'), axis=1)

      # high/low breaches
      self.df.loc[:, self.upper_breach_high()] = self.df.apply(lambda row: self.above_upper_breach(row, 'high'), axis=1)
      self.df.loc[:, self.lower_breach_low()] = self.df.apply(lambda row: self.below_lower_breach(row, 'low'), axis=1)

    def above_upper_breach(self, row, col):
      if row[col] >= row[self.upper_id()]: 
        return 1
      else:
        return 0
    
    def below_lower_breach(self, row, col):
      if row[col] <= row[self.lower_id()]: 
        return 1
      else:
        return 0


