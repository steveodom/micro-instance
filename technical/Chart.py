import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.finance import candlestick_ohlc

from io import BytesIO
from PIL import Image
from s3 import write

class Chart(object):
    def __init__(self, df, frame_size, mode, folder, name, ticker, ylim_stds, parent_df, longterm_frame_size):
        self.df = df
        self.frame_size = frame_size
        self.name = name
        self.folder = folder
        self.ticker = ticker
        self.mode = mode
        self.ylim_stds = ylim_stds

        self.parent_df = parent_df
        self.longterm_frame_size = longterm_frame_size

    def generate(self):
      self.create()
      self.format()
    
      ram = BytesIO()
      plt.savefig(ram, format='png', bbox_inches='tight')
      ram.seek(0)
      im = Image.open(ram)
      im2 = im.convert('RGB').convert('P', palette=Image.ADAPTIVE)
      return im2

    def save(self):  
      #filename = "./images/{0}/{1}/{2}-{3}.png".format(self.mode, self.folder, self.ticker, self.name)
      filename = "./images/manual/{0}_{1}_{2}.png".format(self.folder,self.ticker, self.name)
      print(filename)

      im2 = self.generate()      
      im2.save( filename , format='PNG')

    def save_s3(self):  
      key = "/{1}/{0}_{2}.png".format(self.folder,self.ticker, self.name)
      print(key)
      
      im2 = self.generate()
      write(key, im2)
      
    def create(self):
      self.fig = self.candlestick()
      self.figVol = self.vol()
      self.add_ma()
      self.add_bbands()
      self.add_longterm()
    
    def format(self):
      # self.fig.grid('on')
      self.fig.set_yticklabels([])
      self.fig.set_xticklabels([])
      self.figVol.set_yticklabels([])

      self.fig.tick_params( axis='both', length=0)
      self.figVol.tick_params( axis='both', length=0)

      max = self.df['close'].max()
      min = self.df['close'].min()
      rnge = max - min
      
      diff = ((self.ylim_stds * self.df.iloc[-1]['std']) - rnge)
      
      if diff > 0:
          ymin = min - (diff / 2)
          ymax = max + (diff / 2)
          print('rnge:', rnge, 'diff:', diff, 'max:', max, ymax, 'min:', min, ymin)
          self.fig.set_ylim([ymin,ymax])

    def candlestick(self):
      data = self.df[['id', 'open', 'high', 'low', 'close']].values
      # 6 x 4 grid. start this figure at 0, 0 and span 5 of the 6 rows.
      fig = plt.subplot2grid((6,4), (0,0), rowspan=5, colspan=4, facecolor='#FFFFFF', zorder=0)
      candlestick_ohlc(fig, data, width=.6, colorup='#53c156', colordown='#ff1717')
      return fig

    def vol(self):
      volFig = self.fig.twinx()
      volFig.bar(self.df['id'].values, self.df['vol'].values, color="#CCCCCC", zorder=10, alpha=0.5,)
      return volFig
  
    def add_ma(self):
      #print(self.df['rolling'].values)
      self.fig.plot(self.df['id'].values, self.df['avg'].values, color="yellow", zorder=5)
    
    def add_bbands(self):
      self.fig.plot(self.df['id'].values, self.df['upper'].values, color="orange", zorder=15)
      self.fig.plot(self.df['id'].values, self.df['lower'].values, color="orange", zorder=15)
    
    def add_longterm(self):
      last_id = self.df['id'].iloc[-1]
      start_id = last_id - self.longterm_frame_size
      if start_id < 0:
        start_id = 0
      long_term_frame = self.parent_df.iloc[start_id:last_id,:]
      figLong = plt.subplot2grid((6,4), (5,0), rowspan=1, colspan=4, facecolor='#f9f9f9', zorder=0)
      figLong.plot(long_term_frame['id'].values, long_term_frame['adx'].values, color="blue", zorder=6)
      figLong.plot(long_term_frame['id'].values, long_term_frame['minus_di'].values, color="red", zorder=6)
      figLong.plot(long_term_frame['id'].values, long_term_frame['plus_di'].values, color="green", zorder=6)
      figLong.set_yticklabels([])
      figLong.set_xticklabels([])  
      figLong.tick_params( axis='both', length=0)
    