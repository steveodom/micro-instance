import numbers
from collections import Mapping, Iterable, Set
from decimal import Decimal 
from math import isnan
import boto3
from botocore.exceptions import ClientError
import time
import datetime

from utils.PersistUtils import sanitize_all 
from utils.Logger import Logger

class Persist(object):
    def __init__(self, region_name="us-east-1", table_name=""):
        self.conn = boto3.resource('dynamodb', region_name=region_name)
        if table_name:
          self.table = self.conn.Table(table_name)
        
        self.batch_items = []
        self.logger = Logger('technical').logger
    
    def add_to_batch(self, key, data):
        data = sanitize_all(data)
        self.batch_items.append(data)
          
    def save_as_batch(self, ticker=""):
        """
        Batch write items to given table name
        """
        self.logger.info('saving batch for {}'.format(ticker))
        try:
            with self.table.batch_writer() as batch:
                for item in self.batch_items:
                    batch.put_item(Item=item)
        except ClientError as e:
            self.logger.error("Unexpected error: %s" % e)