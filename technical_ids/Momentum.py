from .QueryUtil import QueryUtil

class Momentum(object):    
    def __init__(self):
      pass
    
    def cci_base(self):
      s = "MOCCI"
      return QueryUtil.object_builder(s,s,s)

    def cci_overbought(self):
      s = "MOCCIOB"
      return QueryUtil.object_builder(s,s,s)
    
    def cci_oversold(self):
      s = "MOCCIOS"
      return QueryUtil.object_builder(s,s,s)

    def moneyflow_base(self):
      s = "MOMFI"
      return QueryUtil.object_builder(s,s,s)

    def moneyflow_overbought(self):
      s = "MOMFIOB"
      return QueryUtil.object_builder(s,s,s)
    
    def moneyflow_oversold(self):
      s = "MOMFIOS"
      return QueryUtil.object_builder(s,s,s)

    def bop_base(self):
      s = "MOBOP"
      return QueryUtil.object_builder(s,s,s)

    def bop_positive(self):
      s = "MOBOPP"
      return QueryUtil.object_builder(s,s,s)
    
    def bop_negative(self):
      s = "MOBOPN"
      return QueryUtil.object_builder(s,s,s)

    def queryables(self):
      return [
        self.cci_overbought(),
        self.cci_oversold(),
        self.moneyflow_overbought(),
        self.moneyflow_oversold(),
        self.bop_positive(),
        self.bop_negative()
      ]