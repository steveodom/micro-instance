from .QueryUtil import QueryUtil

class Bollinger(object):    
    def __init__(self):
      pass

    def upper(self):
      return {"id": "BBU"}
  
    def average(self):
      return {"id": "BBA"}

    def lower(self):
      return {"id": "BBL"}

    def upper_breach_close(self):
      s = "BBUBC"
      return QueryUtil.object_builder(s, s, s)

    def lower_breach_close(self):
      s = "BBLBC"
      return QueryUtil.object_builder(s, s, s)

    def upper_breach_high(self):
      s = "BBUBH"
      return QueryUtil.object_builder(s, s, s)

    def lower_breach_low(self):
      s = "BBLBL"
      return QueryUtil.object_builder(s, s, s)

    def queryables(self):
      return [
        self.upper_breach_close(),
        self.lower_breach_close(),
        self.upper_breach_high(),
        self.lower_breach_low()
      ]
