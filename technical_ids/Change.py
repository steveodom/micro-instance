from .QueryUtil import QueryUtil

class Change(object):    
    def __init__(self):
      self.klass_name = 'Direction'

    def time_direction(self, is_future = True):
      if is_future:
        return 'F'
      else:
        return 'P' 

    def log(self, period, is_future = True):
      d = self.time_direction(is_future)
      # if (period < 0):
      #   period = period * -1
      return "CH{}L{}".format(d, period)

    def zscore(self, period, is_future = True):
      # if (period < 0):
      #   period = period * -1
      return "CH{}Z{}".format(self.time_direction(is_future), period)

    def target_date(self, period, is_future = True):
      return "CH{}D".format(self.time_direction(is_future))
      
    def up(self):
      s = "up"
      return QueryUtil.object_builder(s, "CHU", self.klass_name, s)

    def down(self):
      s = "down"
      return QueryUtil.object_builder(s, "CHD", self.klass_name, s)
      
    def flat(self):
      s = "flat"
      return QueryUtil.object_builder(s, "CHF", self.klass_name, s)

    def queryables(self):
      return [
        self.up(),
        self.down(),
        self.flat()
      ]