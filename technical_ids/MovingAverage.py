from .QueryUtil import QueryUtil

class MovingAverage(object):    
    def __init__(self):
      pass

    def base(self, period):
      return {"id": "MA{}d".format(period)}
    
    def upper_breach_5d_close(self):
      s = "MAUB5"
      return QueryUtil.object_builder(s,s,s)
      
    def upper_breach_200d_close(self):
      s = "MAUB200"
      return QueryUtil.object_builder(s,s,s)

    def lower_breach_5d_close(self):
      s = "MALB5"
      return QueryUtil.object_builder(s,s,s)

    def lower_breach_200d_close(self):
      s = "MALB200"
      return QueryUtil.object_builder(s,s,s)

    def queryables(self):
      return [
        self.upper_breach_5d_close(),
        self.upper_breach_200d_close(),
        self.lower_breach_5d_close(),
        self.lower_breach_200d_close()
      ]
