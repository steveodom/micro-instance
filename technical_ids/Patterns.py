from .QueryUtil import QueryUtil
class Patterns(object):
    
    def __init__(self):
      pass
    
    def twoCrows(self):
      s = "PR2C"
      return QueryUtil.object_builder(s, s, s)
      
    def threeCrows(self):
      s = "PR3C"
      return QueryUtil.object_builder(s, s, s)
  
    def headAndShoulders(self):
      s = "PRHS"
      return QueryUtil.object_builder(s, s, s)

    def headAndShouldersInverted(self):
      s = "PRHSI"
      return QueryUtil.object_builder(s, s, s)

    def doji(self):
      s = "PRDOJI"
      return QueryUtil.object_builder(s, s, s)
  
    def dojiStar(self):
      s = "PRDOJIS"
      return QueryUtil.object_builder(s, s, s)

    def engulfing(self):
      s = "PRENGULF"
      return QueryUtil.object_builder(s, s, s)

    def dragonflyDoji(self):
      s = "PRDRDOJI"
      return QueryUtil.object_builder(s, s, s)

    def eveningDojiStar(self):
      s = "PREVDOJIS"
      return QueryUtil.object_builder(s, s, s)

    def eveningStar(self):
      s = "PREVS"
      return QueryUtil.object_builder(s, s, s)

    def queryables(self):
      return [
        self.threeCrows(),
        self.twoCrows(),
        self.headAndShoulders(),
        self.headAndShouldersInverted(),
        self.doji(),
        self.dojiStar(),
        self.engulfing(),
        self.eveningDojiStar(),
        self.eveningStar(),
        self.dragonflyDoji()
      ]