from .QueryUtil import QueryUtil

class Macd(object):    
    def __init__(self):
      pass

    def base(self):
      return {"id": "MD"}

    def histogram(self):
      return {"id": "MDH"}

    def signal(self):
      return {"id": "MDS"}

    def bull_signal_x(self):
      s = "MDUS"
      return QueryUtil.object_builder(s, s, s)

    def bear_signal_x(self):
      s = "MDDS"
      return QueryUtil.object_builder(s, s, s)

    def bull_center_x(self):
      s = "MDUC"
      return QueryUtil.object_builder(s, s, s)

    def bear_center_x(self):
      s = "MDDC"
      return QueryUtil.object_builder(s, s, s)

    def queryables(self):
      return [
        self.bull_signal_x(),
        self.bear_signal_x(),
        self.bull_center_x(),
        self.bear_center_x()
      ]
  