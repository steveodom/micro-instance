import boto3
client = boto3.client('ec2', region_name='us-east-1')


def stop_instance(instance_id):
    response = client.stop_instances(
        InstanceIds=[instance_id]
    )