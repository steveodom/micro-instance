from datetime import datetime, timedelta
import requests

from .S3 import S3

class Quotes(object):
    def __init__(self, ticker, quote_limit=99):
        self.ticker = ticker
        self.av_key = "0WY3"
        self.tradier_key = "OapehNEkNeJBqFZPIAeV7rg0BPjG"
        self.quote_limit = quote_limit
        quote_start_date = datetime.now() - timedelta(days=quote_limit)
        self.quote_start_date = quote_start_date.strftime("%Y-%m-%d")

    def alphavantage_params(self, period):
        output_size = 'full'
        endpoint = "TIME_SERIES_INTRADAY"
        key = "Time Series (5min)"

        if period == '3y' or period == '1m' or period == '1d' or period == '1m':
          endpoint = "TIME_SERIES_DAILY"
          key = "Time Series (Daily)"

        if period == '1d' or period == '1m':
          output_size = 'compact'
      
        path = "query?function={0}&symbol={1}&interval=5min&apikey={2}&outputsize={3}".format(endpoint, self.ticker, self.av_key, output_size)
        return path, key
       
    def fetch(self, period = "5min"):
        print('fetching...', self.ticker)
        path, key = self.alphavantage_params(period)
        url = 'https://www.alphavantage.co/{0}'.format(path)

        req = requests.get(url)
        print(req.status_code, 'status_code')
        if req.status_code == 200:
          res = req.json()
        else:
          print('problem retrieving the quote')
          return []

        if key in res:
          return res[key]
        else:
          print('does not have key')
          return []

    def bad_response(self):
        print('problem retrieving the quote')
        return None
    
    def format_tradier(self, res):
        obj = {}
        for i, quote in enumerate(res):
          if i < self.quote_limit:
            obj[quote['date']] = quote
        return obj
      
    def fetch_tradier(self, period="daily"):
        print('fetching...', self.ticker)
        headers = {
          'Authorization': 'Bearer {}'.format(self.tradier_key),
          'Accept': 'application/json'
        }
        url = 'https://api.tradier.com/v1/markets/history?symbol={0}&interval={1}&start={2}'.format(self.ticker, period, self.quote_start_date)
        
        req = requests.get(url, headers=headers)
        print(req.status_code, 'status_code')
        if req.status_code == 200:
          res = req.json()
          if 'history' in res:
            history = res['history']
            if history is not None and 'day' in history:
                history['day'].reverse()
                return self.format_tradier(history['day'])
                #return history['day']
            else:
                return self.bad_response()  
          else:
            return self.bad_response()
        else:
          return self.bad_response()

    
    def fetch_cached(self):
        frame_size_in_minutes = 5
        return S3(self.quote_limit, frame_size_in_minutes).fetch(self.ticker)