import pandas as pd 
import numpy as np
from scipy.stats import zscore, linregress
from .main import Quotes

class Dataframe(object):
    def __init__(self, quote_type, ticker, max_length=104):
        self.ticker = ticker
        self.quote_type = quote_type
        self.max_length = max_length

    def populate_dataframe(self):
        if self.quote_type == 'daily':
            res = Quotes(self.ticker, self.max_length).fetch_tradier('daily')
            if res is not None:
                print('success')
                return pd.DataFrame(res)
            else:
                return None
        else:
            res = Quotes(self.ticker, self.max_length).fetch_cached()
            #print(res.tail())
            return res
    
    def calcSlope(self, row, col):
        window_length = 5
        rid = row.date
        id = self.df.index.get_loc(self.df.loc[rid]['date']) + 1
        if id >= window_length:
            frame = self.df.iloc[id-window_length:id,:]

            slope = linregress(frame['close'], np.arange(window_length)).slope
            #print(frame[['close', 'date']], slope, rid, id)
            return slope
        else:
            return 0

    def convert(self):
        self.df.loc[:,'open'] = self.df['open'].astype(float)
        self.df.loc[:,'close'] = self.df['close'].astype(float)
        self.df.loc[:,'high'] = self.df['high'].astype(float)
        self.df.loc[:,'low'] = self.df['low'].astype(float)
        #self.df.loc[:,'volume'] = self.df['5. volume'].astype(int)
        #self.df.drop(['high', 'low', 'volume'], axis=1, inplace=True)
        
    def supplement(self, lag):
        self.df['log_ret'] = np.log(self.df['close']) - np.log(self.df['close'].shift(1))
        self.df["C/O"] = self.df["close"]/self.df["open"]
        self.df['log_zscore'] = self.df[['log_ret']].apply(zscore)
        # added exponential weight average to dampen returns to tighten correlations.
        self.df['ewm'] = self.df['log_ret'].ewm(span=6).mean()
        self.df.loc[:,'slope'] = self.df.apply(lambda row: self.calcSlope(row, 'close'), axis=1)
        self.df['delta'] = np.log(self.df['close']) - np.log(self.df['close'].shift(lag))
        self.df.loc[:,'direction'] = self.df.apply(lambda row: 1 if row['delta'] > 0 else -1, axis=1)

    def clean(self):
        self.df['log_ret'].fillna(0, inplace=True)
        self.df['ewm'].fillna(0, inplace=True)
        self.df['log_zscore'].fillna(0, inplace=True)
        self.df['delta'].fillna(0, inplace=True)

    def build(self, lag=-5):
      self.df = self.populate_dataframe()
      if self.df is not None:
        if self.quote_type == 'daily':
            self.df = self.df.transpose()
        self.convert()
        # self.supplement(lag)
        # self.clean()
        #print(self.df.head())
        return self.df


