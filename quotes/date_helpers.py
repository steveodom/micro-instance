import datetime

end_time = datetime.datetime.strptime( "16:00:00", '%H:%M:%S')
start_time = datetime.datetime.strptime( "08:30:00", '%H:%M:%S')

# not used
def total_frames_in_trading_day(frame_size_in_minutes=5):
    seconds = (end_time-start_time).total_seconds()
    minutes = seconds / 60
    total_frames = minutes / frame_size_in_minutes
    return total_frames

def ticks_remaining_to_time(frames_remaining, frame_size_in_minutes=5):    
    minutes_remaining = int(frames_remaining) * frame_size_in_minutes
    time = end_time - datetime.timedelta(minutes=minutes_remaining)
    return time

def get_date_from_day_of_year(day_of_year):
    return datetime.datetime.strptime(str(day_of_year), "%j")

def convert_prefix_to_time(yr, days_left_in_year, ticks_remaining, frame_size_in_minutes):
    day_of_year = 365 - int(days_left_in_year)
    date_of_trade = get_date_from_day_of_year(day_of_year)
    day = date_of_trade.day
    month = date_of_trade.month
    
    tt = ticks_remaining_to_time(ticks_remaining, frame_size_in_minutes)
    h = tt.hour
    minute = tt.minute
    date_str = "{0}_{1}_{2}-{3}:{4}--0400".format(month, day, yr, h, minute)
    dt = datetime.datetime.strptime( date_str, '%m_%d_%y-%H:%M-%z')
    return {'date_str': date_str, 'date_obj': dt }
