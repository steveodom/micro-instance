import pandas as pd
import boto3

from .date_helpers import convert_prefix_to_time

client = boto3.client('s3', 
    aws_access_key_id='AKIAJRQR2MXUUW3WE6ZA',
    aws_secret_access_key='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',
    region_name='us-east-1'
)

def write(key, body):
  client.put_object(
    Bucket='trading-logs-trades',
    Key=key,
    Body=body
  )

class S3(object):
    def __init__(self, max_quotes, frame_size_in_minutes):
      self.paginator = client.get_paginator('list_objects')
      self.max_quotes = max_quotes
      self.bucket = 'trading-logs-trades'
      self.frame_size_in_minutes = frame_size_in_minutes

  
    def fetch(self, ticker):
      page_iterator = self.paginator.paginate(
        Bucket=self.bucket, 
        Prefix="quotes/{0}/".format(ticker), 
        PaginationConfig={'MaxItems': 20000}
      )

      rows_list = []
      i = 0
      for page in page_iterator:
          files = page['Contents']
          for details in files:
              key = details['Key']
              try:
                keyString, close, high, low, open_price, volume = key.split('-', 6)
                ignore, ticker, custom_time_object  = keyString.split('/',3)

                yr, days_left_in_year, ticks_remaining = custom_time_object.split(':')
                t = convert_prefix_to_time(yr, days_left_in_year, ticks_remaining, self.frame_size_in_minutes)
                obj = {
                  'date': t['date_obj'], 
                  'open': float(open_price), 
                  'high': float(high), 
                  'low': float(low), 
                  'close': float(close), 
                  'volume': int(volume)
                }
                rows_list.append(obj)
                i += 1
              except:
                  False
                  print(key)

                  
      df = pd.DataFrame(
        data=rows_list, 
        columns=('date', 'open', 'high', 'low', 'close', 'volume')
      )
      pd.to_datetime(df.date)
      #print(df.tail())
      df.sort_values(['date'], ascending=True, inplace=True)
      return df.iloc[-self.max_quotes:,:]