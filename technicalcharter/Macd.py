import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from .ChartUtil import ChartUtil
from .Persist import Persist


class Macd(object):
  
    def __init__(self, df, ticker, frame_size):
        self.df = df
        self.label_spacing = 8
        self.frame_size = frame_size
        self.ticker = ticker
    
    def chart(self, start_i, save_location='location', simple_only=False, prefix='indicators'):
        label_spacing = 8
        start_pos = start_i + 1
        end_pos = start_pos - self.frame_size + 1

        frame = self.df[end_pos:start_pos]
        primary = frame.plot(kind='line', y='MD', secondary_y=True, x='date', rot=-45, legend=False, color="#2196F3", linewidth=2.5)
        secondary = frame['MDH'].plot(kind='bar', color='#BBDEFB', width=0.70) #k is black
        frame['MDS'].plot(kind='line', color='#ff4081', secondary_y=True, linewidth=1)
        
        if simple_only:
            ChartUtil.simple_chart(plt, primary, secondary)
        else:
            ticks = primary.xaxis.get_ticklocs()
            ticklabels = [l.get_text() for l in primary.xaxis.get_ticklabels()]
            primary.xaxis.set_ticks(ticks[::label_spacing])
            primary.xaxis.set_ticklabels(ticklabels[::label_spacing])
        
        end_date = frame.iloc[-1]['date']

        persist = Persist(self.ticker, end_date, 'macd')
        if save_location == 's3':
            ram = ChartUtil.generate_s3(plt)
            persist.save_s3(ram, prefix)
        else:
            im2 = ChartUtil.generate(plt)
            persist.save(im2, prefix)
        plt.close()
