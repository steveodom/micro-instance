# should use as much stuff from /technical as possible
# since these charts support that data.
from technical.Builder import Builder
from technical.config import TICKERS, NUMBER_OF_QUOTE_PERIODS
from .Macd import Macd

BATCH_LIMIT = 499

def run(trigger_source):
    quote_period = 'daily'
    frame_size = 45
    
    for ticker in TICKERS:
      for ticker in TICKERS:
        df = Builder(quote_period, NUMBER_OF_QUOTE_PERIODS).format(ticker)
        macd = Macd(df, ticker, frame_size)
        for date, row in df.iterrows():
            print(date, row['id'])
            if row['id'] > frame_size:
                macd.chart(row['id'])
    
    print('complete')