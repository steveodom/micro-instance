from .s3 import write

class Persist(object):
  
    def __init__(self, ticker, end_date, chart_type):
        self.ticker = ticker
        self.end_date = end_date
        self.chart_type = chart_type

    def save(self, im2, prefix):  
      filename = "./technicalcharter/images/updown/{}_{}_{}_{}.png".format(prefix, self.ticker, self.end_date, self.chart_type)
      print(filename)
      im2.save( filename , format='PNG')

    def save_s3(self, im2, prefix):  
      key = "charts/{}/{}/{}_{}.png".format(prefix, self.ticker, self.end_date, self.chart_type)
      print(key)
      write(key, im2)