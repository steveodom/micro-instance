import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from .ChartUtil import ChartUtil
from .Persist import Persist


class Price(object):
  
    def __init__(self, df, ticker, frame_size):
        self.df = df
        self.label_spacing = 8
        self.frame_size = frame_size
        self.ticker = ticker
    
    def chart(self, start_i, save_location='location', simple_only=False, prefix=''):
        label_spacing = 8
        start_pos = start_i + 1
        end_pos = start_pos - self.frame_size + 1

        frame = self.df[end_pos:start_pos]
        primary = frame.plot(kind='line', y='close', x='date', rot=-45, legend=False, style='k', linewidth=4.0)
        frame['BBU'].plot(kind='line', style='r', linewidth=1)
        frame['BBL'].plot(kind='line', style='r', linewidth=1)

        if simple_only:
            ChartUtil.simple_chart(plt, primary, None)
        else:
            ticks = primary.xaxis.get_ticklocs()
            ticklabels = [l.get_text() for l in primary.xaxis.get_ticklabels()]
            primary.xaxis.set_ticks(ticks[::label_spacing])
            primary.xaxis.set_ticklabels(ticklabels[::label_spacing])
        
        end_date = frame.iloc[-1]['date']

        persist = Persist(self.ticker, end_date, 'price')
        if save_location == 's3':
            ram = ChartUtil.generate_s3(plt)
            persist.save_s3(ram, prefix)
        else:
            im2 = ChartUtil.generate(plt)
            persist.save(im2, prefix)
        plt.close()
