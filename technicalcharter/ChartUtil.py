from io import BytesIO
from PIL import Image

class ChartUtil(object):

    @staticmethod
    def base_generate(plt):
        ram = BytesIO()
        plt.savefig(ram, format='png', bbox_inches='tight')
        ram.seek(0)
        return ram
    
    @staticmethod    
    def generate(plt):
        ram = ChartUtil.base_generate(plt)
        im = Image.open(ram)
        im2 = im.convert('RGB').convert('P', palette=Image.ADAPTIVE)
        return im2

    @staticmethod
    def generate_s3(plt):
        ram = ChartUtil.base_generate(plt)
        return ram

    @staticmethod
    def simple_chart(plt, primary, secondary):
        primary.axis('off')
        primary.xaxis.set_visible(False)
        primary.yaxis.set_visible(False)
        
        if secondary:
            secondary.axis('off')
            secondary.xaxis.set_visible(False)
            secondary.yaxis.set_visible(False)


        fig_size = plt.rcParams["figure.figsize"]
        
        fig_size[0] = 4
        fig_size[1] = 4
    


    