
import time
import datetime

def string_date_to_number(date_str):
    return int(datetime.datetime.strptime(date_str, "%Y-%m-%d").timestamp())

def time_now():
    return datetime.datetime.now().strftime("%y-%m-%d-%H-%M")