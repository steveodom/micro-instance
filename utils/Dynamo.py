import boto3
from time import sleep

class Dynamo(object):
    def __init__(self, region_name="us-east-1", table_name="TechnicalHistoryTable"):
        self.conn = boto3.resource('dynamodb', region_name=region_name)
        self.table_name = table_name
        self.table = self.conn.Table(self.table_name)
        
    def ensure_throughput_is_different(self, read, write):
        tp = self.table.provisioned_throughput
        current_read = tp['ReadCapacityUnits']
        current_write = tp['WriteCapacityUnits']
        if current_read == read:
          read += 1
        
        if current_write == write:
          write += 1
        
        return read, write
    
    def update_throughput(self, read = 5, write = 5, wait = True):
        read, write = self.ensure_throughput_is_different(read, write)
        self.table.update(
          ProvisionedThroughput={
            'ReadCapacityUnits': read,
            'WriteCapacityUnits': write
          }
        )
      
        if wait:
          while self.table.table_status == "UPDATING":
            self.table = self.conn.Table(self.table_name)
            sleep(5)
    
    def update_secondary_throughput(self, index_name, read = 5, write = 5, wait = True):
        read, write = self.ensure_throughput_is_different(read, write)
        self.table.update(
          GlobalSecondaryIndexUpdates=[
            {
              "Update": { 
                "IndexName": index_name,
                "ProvisionedThroughput": {
                  'ReadCapacityUnits': read,
                  'WriteCapacityUnits': write
                }
              }
            }
          ]
        )
      
        if wait:
          while self.table.table_status == "UPDATING":
            self.table = self.conn.Table(self.table_name)
            sleep(5)