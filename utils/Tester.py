import boto3
from botocore.exceptions import ClientError
import datetime
from .Logger import Logger



def run():
  log = Logger('tester')
  logger = log.logger
  #log.debug()
  try:
    conn = boto3.resource('dynamodb', region_name="us-east-1")    
    table = conn.Table("TechnicalInstancesTable")
    d = datetime.datetime.now().strftime("%y%m%d%H%M")
    Item = {"id": "STO", "date_number": int(d)}
    
    x = table.put_item(Item=Item)
    logger.info(x)
  except ClientError as e:
    logger.error("Unexpected error: %s" % e)
  