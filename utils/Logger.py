import logging

class Logger(object):
    def __init__(self, app="UndefinedApp"):
      logging.basicConfig(level=logging.INFO)
      logger = logging.getLogger(__name__)
      
      # create a file handler
      self.handler = logging.FileHandler("./logs/{}.log".format(app))
      self.handler.setLevel(logging.INFO)

      # create a logging format
      formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
      self.handler.setFormatter(formatter)

      # add the handlers to the logger
      logger.addHandler(self.handler)

      self.logger = logger

    def debug(self):
      self.handler.setLevel(logging.DEBUG)
      self.logger.setLevel(logging.DEBUG)
      
      
        
