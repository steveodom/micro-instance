from boto3.dynamodb.conditions import Key, Attr

class QueryUtil(object):
    
    @staticmethod
    def eq(key, val):
      # 1 for bool positive. Not a string
      return Key(key).eq(val)

    @staticmethod
    def gt(key, val):
      # 1 for bool positive. Not a string
      return Key(key).gt(val)
  
    @staticmethod
    def lt(key, val):
      # 1 for bool positive. Not a string
      return Key(key).lt(val)

    @staticmethod
    def find_opposite_id(id):
      if id.startswith("CHD"):
        replace = "CHD"
        replace_with = "CHU"
      elif id.startswith("CHU"):
        replace = "CHU"
        replace_with = "CHD"
      else: replace = None

      if replace is not None:
        opp_id = id.replace(replace, replace_with)
      else:
        opp_id = None
      return opp_id