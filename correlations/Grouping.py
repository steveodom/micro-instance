import numpy as np
import scipy.stats
from scipy.cluster.vq import kmeans, whiten, vq
from scipy.stats import linregress

from .Regression import Regression

class Grouping(object):
    def __init__(self, tickers, quotes, data, feature_matrix, group_lists, number_of_groups, Highs):
        self.tickers = tickers
        self.quotes = quotes
        self.data = data
        self.feature_matrix = feature_matrix
        self.group_lists = group_lists
        self.number_of_groups = number_of_groups
        self.Highs = Highs

    def add_group_index_per_ticker(self, ticker, group, group_id, calc):
        if ticker in self.quotes:
            quote = self.quotes[ticker][calc['quote_key']]
            ary = self.period_average_for_group(ticker, group, calc['quote_key'])

            if ary is not None and len(quote) > calc['window_length']:
                rvalue = Regression.calc_r2(quote, ary, calc['lag'], calc['window_length'])
            else:
                rvalue = 0
            self.data[ticker]["group_correlations"][group_id][calc['name']] = rvalue
            self.Highs.record_overall(calc['name'], "groups", rvalue, ticker, group_id)

    def period_average_for_group(self, ticker, group, quote_key):
          siblings = list(group)
          if ticker in siblings:
            siblings.remove(ticker)
          ary = []
          
          if siblings is not None:
              for sibling in siblings:
                  target = self.quotes[sibling]
                  val = np.nan_to_num(target[quote_key].values, copy=False)
                  ary.append(val)
          ar = np.array(ary)
          if ar.size > 0:
              return ar.mean(axis=0)
          else:
              return None
    
    def kmeans(self, name):
        W = whiten(self.feature_matrix[name])
        centroids, variance = kmeans(W, self.number_of_groups)
        groups, distance = vq(W, centroids)
    
        for group, ticker in zip(groups, self.tickers):
            safe_group = str(group.item()) # group is a numpy int64. item() converts it to native python int
            if safe_group not in self.group_lists[name]:
                self.group_lists[name][safe_group] = []
            if ticker in self.data:
                self.group_lists[name][safe_group].append(ticker)
                self.data[ticker]["group"][name] = safe_group

