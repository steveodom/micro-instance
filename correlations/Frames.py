
class Frames(object):
    def __init__(self, df, frame_size):
        self.frame_size = frame_size
        self.windows = {}
        self.df = df

    def insert_id(self):
        self.df.insert(0, 'id', range(0, len(self.df)))

    def create_frame(self, i):
        mmax = self.df['id'] < i
        mmin = self.df['id'] >= i - self.frame_size
        window = self.df[mmin & mmax].copy()
        if not window.empty:
          last_date = window.iloc[-1,:].date
          self.windows[last_date] = window

    def create(self):
        for date, row in self.df.iterrows():
            self.create_frame(row['id'])
        return self.windows

    