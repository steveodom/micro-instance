from datetime import datetime 

from quotes.Dataframe import Dataframe
from .Builder import Builder
from .Frames import Frames
from .Firestore import Firestore
from .Persist import Persist

class Row(object):
    def __init__(self, quote_type, frame_size, number_of_quotes = 10000):
        self.windows = {}
        self.quote_type = quote_type
        self.frame_size = frame_size
        self.number_of_quotes = number_of_quotes

    def add_frame_for_ticker(self, ticker, quote_df):
        frames = Frames(quote_df, self.frame_size)
        frames.insert_id()
        self.windows[ticker] = frames.create()

    def create_frames(self, tickers, lag=5):
        for ticker in tickers:
            quote_df = Dataframe(self.quote_type, ticker, self.number_of_quotes).build(lag)
            if quote_df is not None:
              self.add_frame_for_ticker(ticker, quote_df)
        
    def log_current_time(self, trigger_source):
        #fs = Firestore(self.quote_type, 'correlations-time', False)
        #fs.save('updates', {"last_update": datetime.now().strftime("%Y-%m-%d %H:%M"), "source": trigger_source})
        pass
    
    def is_most_recent_date(self, date, frames):
        # this might be error prone. Dicts aren't ordered
        last_frame_date = list(frames.keys())[-1]
        return last_frame_date == date

    def create(self, tickers, calc_periods, number_of_groups, trigger_source):
        self.log_current_time(trigger_source)
        self.create_frames(tickers, calc_periods[0]['lag'])
        
        any_ticker = tickers[0]
        frames = self.windows[any_ticker]
        persist = Persist(number_of_groups)
        for i, end_date in enumerate(frames):
            print(end_date, 'enddate')
            builder = Builder(self.quote_type, tickers, number_of_groups, self.windows)
            builder.build(end_date, calc_periods)
            persist.data_to_df(builder.data,calc_periods, end_date)

        persist.data_to_csv()

    


        
