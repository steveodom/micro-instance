import pandas as pd 
import numpy as np

class Persist(object):
    def __init__(self, number_of_groups):
      self.data_tickers = {}
      self.group_memberships_tickers = {}
      self.highs_tickers = {}
      self.group_correlations_tickers = {}
      self.data_directory = "data/correlations"
      self.should_compress = False
      self.number_of_groups = number_of_groups

      self.window_length_per_period = {}
    
    def init_data_ticker(self, ticker, calc_periods):
        if ticker not in self.data_tickers:
              self.data_tickers[ticker] = {}
              for period in calc_periods:
                name = period['name']
                self.data_tickers[ticker][name] = {}
                self.window_length_per_period[name] = period['window_length']
    
    def init_group_membership(self, ticker, calc_periods):
        if ticker not in self.group_memberships_tickers:
              self.group_memberships_tickers[ticker] = {}
              for period in calc_periods:
                name = period['name']
                self.group_memberships_tickers[ticker][name] = {}

    def init_highs_ticker(self, ticker, calc_periods):
        if ticker not in self.highs_tickers:
              self.highs_tickers[ticker] = {}
              for period in calc_periods:
                name = period['name']
                self.highs_tickers[ticker][name] = {}
    
    def init_group_correlations(self, ticker, calc_periods):
        if ticker not in self.group_correlations_tickers:
              self.group_correlations_tickers[ticker] = {}
              for period in calc_periods:
                name = period['name']
                self.group_correlations_tickers[ticker][name] = {}
                for group_id in np.arange(0,self.number_of_groups):
                  self.group_correlations_tickers[ticker][name][str(group_id)] = {}


    def data_to_df(self, builder_data, calc_periods, end_date):
        for ticker in builder_data:
          data = builder_data[ticker]
          self.init_data_ticker(ticker, calc_periods)
          #self.init_group_membership(ticker, calc_periods)          
          self.init_highs_ticker(ticker, calc_periods)
          #self.init_group_correlations(ticker, calc_periods)    

          for period in calc_periods:
            name = period['name']
            if name in data:
              corrs = data[period['name']]
              #group_membership = data['group'][name]
              high = data['highs'][name]
              
              self.data_tickers[ticker][name][end_date] = corrs
              #self.group_memberships_tickers[ticker][name][end_date] = group_membership
              self.highs_tickers[ticker][name][end_date] = high

              # for group_id in data['group_correlations']:
              #   if group_id in data['group_correlations'] and name in data['group_correlations'][group_id]:
              #     group_correlation = data['group_correlations'][group_id][name]
              #     if end_date not in self.group_correlations_tickers[ticker][name]:
              #       self.group_correlations_tickers[ticker][name][end_date] = {}
                  
              #     self.group_correlations_tickers[ticker][name][end_date][str(group_id)] = group_correlation


    def write_ticker_correlations(self):
        for ticker in self.data_tickers:
          target = self.data_tickers[ticker]
          for period in target:
            df = pd.DataFrame(target[period]).transpose()
            if not df.empty:
              columns = list(df.columns)
              wl = self.window_length_per_period[period]
              filename = "{0}-{1}-{2}len".format(ticker,period,wl)
              self.write_df(self.data_directory,filename,df.round(3), columns)
    
    def write_group_membership_history(self):
        for ticker in self.group_memberships_tickers:
          target = self.group_memberships_tickers[ticker]
          
          for period in target:
            df = pd.DataFrame(list(target[period].items()))
            if not df.empty:
              df.columns = ['date', 'group']
              columns = list(df.columns)
              wl = self.window_length_per_period[period]
              filename = "{0}-{1}-{2}len-group-membership".format(ticker,period, wl)
              self.write_df(self.data_directory,filename,df,columns)
    
    def write_highs_history(self):
        for ticker in self.highs_tickers:
          target = self.highs_tickers[ticker]
          
          for period in target:
            df = pd.DataFrame(target[period]).transpose()
            if not df.empty:
              columns = list(df.columns)
              wl = self.window_length_per_period[period]
              filename = "{0}-{1}-{2}len-high-ticker".format(ticker,period,wl)
              self.write_df(self.data_directory,filename,df.round(2),columns)
    
    def write_group_correlations_history(self):
        for ticker in self.group_correlations_tickers:
          target = self.group_correlations_tickers[ticker]
          for period in target:
            df = pd.DataFrame(target[period]).transpose()
            
            df.drop(df.index[[0,1]], inplace=True)
            if not df.empty:
              columns = list(df.columns)
              wl = self.window_length_per_period[period]
              filename = "{0}-{1}-{2}len-group-correlation".format(ticker,period,wl)
              self.write_df(self.data_directory,filename,df,columns)

    def data_to_csv(self):  
        self.write_ticker_correlations()  
        #self.write_group_membership_history()
        self.write_highs_history()
        #self.write_group_correlations_history()
                
    def write_df(self, directory, filename, df, columns):
        key = "{0}/{1}.csv".format(directory, filename)
        if self.should_compress:
          df.to_csv(key, compression='gzip', columns=columns)
        else:
          df.to_csv(key, columns=columns)
    