from datetime import datetime 

from .Row import Row
from .config import TICKERS, NUMBER_OF_GROUPS_TO_USE, NUMBER_OF_QUOTE_PERIODS
from .Firestore import Firestore
from .FetchQuotes import FetchQuotes

def run(trigger_source):
    fs = Firestore('aws', 'log', False).save("correlations", {"last_update": datetime.now().strftime("%Y-%m-%d %H:%M"), "source": trigger_source})
    calc_periods = [
        {"name": "2_periods", "quote_key": "direction", "lag": -2, "window_length": 25},
        {"name": "5_periods", "quote_key": "direction", "lag": -5, "window_length": 25}
    ]
    
    frame_size = max(period['window_length'] for period in calc_periods) + 1
    Row('daily', frame_size, NUMBER_OF_QUOTE_PERIODS).create(TICKERS, calc_periods, NUMBER_OF_GROUPS_TO_USE, trigger_source)
