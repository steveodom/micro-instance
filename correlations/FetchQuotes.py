from quotes.Dataframe import Dataframe

class FetchQuotes(object):
    def __init__(self, quote_type):
        self.quotes = {}
        self.quote_type = quote_type

    def go(self, tickers):
      for ticker in tickers:
            self.add_quote(ticker)
      return self.quotes
    
    def add_quote(self, ticker):
        quote = Dataframe(self.quote_type, ticker).build()
        if quote is not None:
            self.quotes[ticker] = quote
