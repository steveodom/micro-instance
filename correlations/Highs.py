

class Highs(object):
    def __init__(self, data):
      self.highs = {}
      self.data = data
    
    def init_overall_high_tracker(self, name):
        base_high = {"base_ticker": "", "target_ticker": "", "value": 0}
        self.highs.update({
            name: {
                "tickers": base_high,
                "groups": base_high
            }
        })

    def record_overall(self, name, klass, rvalue, ticker, target):
        if rvalue > self.highs[name][klass]["value"]:
            self.highs[name][klass] = {
                "base_ticker": ticker,
                "target_ticker": target,
                "value": rvalue
            }

    def record_ticker_high(self, name, rvalue, ticker, target):
        highs = self.data[ticker]["highs"]
        if rvalue > highs[name]["value"]:
            highs[name] = {
                "value": rvalue,
                "target_ticker": target
            }