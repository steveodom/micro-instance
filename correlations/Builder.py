from datetime import datetime
import numpy as np

from quotes.Dataframe import Dataframe
from .Regression import Regression
from .Grouping import Grouping
from .Highs import Highs

class Builder(object):
    def __init__(self, quote_type, tickers, number_of_groups, quote_frames):
        self.number_of_groups = number_of_groups
        self.sorted_tickers = sorted(tickers)
        ticker_count = len(tickers)
        self.feature_matrix_base = np.zeros([ticker_count, ticker_count])
        self.feature_matrix = {}

        self.quote_frames = quote_frames
        self.quotes = {}
        self.data = {}
        self.group_lists = {}
    
        self.base_quote_details = {
            "group": {},
            "last_update": datetime.now().strftime("%Y-%m-%d %H:%M"),
            "highs": {},
            "group_correlations": self.base_group_correlation_object()
        }
        self.quote_type = quote_type # used to create a different collection

    def base_group_correlation_object(self):
        d = {}
        for group_id in np.arange(0,self.number_of_groups):
            d[str(group_id)] = {}
        return d

    def init_data_objects(self, calc_periods):
        self.calc_periods = calc_periods
        self.Highs = Highs(self.data)
        for calc in calc_periods:
            self.feature_matrix[calc['name']] = self.feature_matrix_base
            self.group_lists[calc['name']] = {}
            self.Highs.init_overall_high_tracker(calc['name'])
        
        

    def build(self, end_date, calc_periods):
        self.init_data_objects(calc_periods)
        self.fetch_quotes(end_date)
        self.regressions()
        #self.group()
        #self.index_regressions()
        #self.save()

        print('completed')

    def fetch_quotes(self, end_date):
        for ticker in self.sorted_tickers:
            self.add_quote(ticker, end_date)
    
    def add_quote(self, ticker, end_date):
        if ticker in self.quote_frames and end_date in self.quote_frames[ticker]:
            quote = self.quote_frames[ticker][end_date]
            if quote is not None:
                self.quotes[ticker] = quote
                self.data[ticker] = self.base_quote_details.copy() 
                self.data[ticker].update({
                    "quotes": quote['close'].values.tolist()
                })
    
    def index_regressions(self):
        grouping = Grouping(self.sorted_tickers, self.quotes, self.data, self.feature_matrix, self.group_lists, self.number_of_groups, self.Highs)
        for calc in self.calc_periods:
            group_list = self.group_lists[calc['name']]

            for group_id in group_list:
                group = group_list[group_id]
                for ticker in self.sorted_tickers:
                    grouping.add_group_index_per_ticker(ticker, group, group_id, calc)
            
    def regressions(self):
        regress = Regression(self.sorted_tickers, self.quotes, self.data, self.feature_matrix, self.Highs)
        for calc in self.calc_periods:
            regress.by_ticker(calc['name'], calc['quote_key'], calc['lag'], calc['window_length'])
    
    def group(self):
        grouping = Grouping(self.sorted_tickers, self.quotes, self.data, self.feature_matrix, self.group_lists, self.number_of_groups, self.Highs)
        for calc in self.calc_periods:
            grouping.kmeans(calc['name'])

    def save(self):
        fs = Firestore(self.quote_type, 'correlations', True)
        for ticker in self.data:
            data = self.data[ticker]
            fs.save_as_batch(ticker, data)
        fs.commit_batch()

        fs = Firestore(self.quote_type, 'correlation-groups', True)
        for name in self.group_lists:
            data = self.group_lists[name]
            fs.save_as_batch(str(name), {'groups': data})
        fs.commit_batch()

        fs = Firestore(self.quote_type, 'correlation-highs', True)
        for name in self.Highs.highs:
            data = self.Highs.highs[name]
            fs.save_as_batch(str(name), data)
        fs.commit_batch()