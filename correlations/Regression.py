from datetime import datetime
from scipy.stats import linregress

class Regression(object):
    def __init__(self, tickers, quotes, data, feature_matrix, Highs):
        self.sorted_tickers = tickers
        self.window_length = 97        
        self.quotes = quotes
        self.data = data
        self.Highs = Highs
        self.feature_matrix = feature_matrix
    
    def init_rvalue_by_ticker_tracker(self, name, ticker):
        highs = self.data[ticker]["highs"].copy()
        highs.update({name: {"target_ticker": "", "value": 0}})
        self.data[ticker].update({
          name: {},
          "highs": highs,
        })
    
    def by_ticker(self, name = "rvalues", quote_key="C/O", lag=-1, window_length=97):
        for i, ticker in enumerate(self.sorted_tickers):
            if ticker in self.quotes and len(self.quotes[ticker]) >= int(window_length):
              self.init_rvalue_by_ticker_tracker(name, ticker)
              self.add_linregress_of_siblings(ticker, i, name, quote_key, lag, window_length)
    
    @staticmethod
    def ensure_equal_dimensions(base_data, target_data, offset, window_length):
        start = Regression.create_offset(offset, window_length)
        base = base_data[-1*window_length::]
        target = target_data[start:offset]
        if len(target) == len(base):
            return base, target
        else:
            if len(base) > len(target):
                base = base_data[-1*len(target)::]
            else:
                start = Regression.create_offset(offset, len(base_data))
                target = target_data[start:offset]
            return base, target

    @staticmethod
    def calc_r2(base_data, target_data, lag, window_length, should_print=False):
        if lag is 0:
          r = linregress(base_data[-1*window_length::], target_data[-1*window_length::]).rvalue
        else:
          base, target = Regression.ensure_equal_dimensions(base_data, target_data, lag, window_length)
          r = linregress(base, target)
          if should_print:
            print(r.rvalue, '------------->', [ round(elem, 4) for elem in base.tolist() ], '----->', [ round(elem, 4) for elem in target.tolist() ] )
          r = r.rvalue
        return r # ** 2
    
    @staticmethod
    def create_offset(lag, window_length):
        return lag - window_length

    def add_linregress_of_siblings(self, ticker, i, name = "rvalues", quote_key="C/O", lag=-1, window_length=97):
        if ticker in self.quotes:
            quote = self.quotes[ticker]
            for j, target in enumerate(self.sorted_tickers):
                if ticker == target:
                    self.data[ticker][name][target] = 1
                    continue
                else:
                    if target in self.quotes:
                        target_quote = self.quotes[target]
                        print(ticker, target, '=----->')
                        should_print = True if ticker == 'COP' and target == 'EMR' else False
                        rvalue = self.calc_r2(quote[quote_key], target_quote[quote_key], lag, window_length, should_print)
                        
                        self.data[ticker][name][target] = rvalue
                        self.Highs.record_overall(name, "tickers", rvalue, ticker, target)
                        self.Highs.record_ticker_high(name, rvalue, ticker, target)
                        
                        self.feature_matrix[name][i][j] = rvalue


      