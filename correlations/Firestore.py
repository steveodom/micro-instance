from google.cloud import firestore


class Firestore(object):
    def __init__(self, period, collection, batch = True):
        db = firestore.Client.from_service_account_json('./trading-v2-64c7b7f0a13e.json')
        coll = "{0}-{1}".format(period, collection)
        self.ref = db.collection(coll)
        if batch:
          self.batch = db.batch()

    def fetch_by_ticker(self, ticker):
        return self.ref.document(ticker).get().to_dict()

    def save(self, field, data):
        return self.ref.document(field).set(data)

    def save_as_batch(self, ticker, data):
        doc_ref = self.ref.document(ticker)
        self.batch.set(doc_ref, data)

    def commit_batch(self):
        self.batch.commit()




# d = Firestore().fetch_by_ticker('msft_2')
# print(d)