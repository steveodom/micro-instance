from datetime import datetime
from pytz import timezone

class Scheduler(object):
  def __init__(self):
      False

  def is_weekday(self, d):
    # weekday returns 0-6. 5 and 6 are Sat and Sun.
    return d.weekday() < 5

  def convert_now(self):
    tz = timezone('America/Chicago')
    austin_now = datetime.now(tz)
    return self.is_weekday(austin_now), austin_now.hour

  def should_run(self, config):
    is_weekday, hour_now = self.convert_now()
    print(is_weekday, hour_now, 'hour now')
    return (config['run_on_weekdays'] and is_weekday) and (config['hour_to_run'] == hour_now)