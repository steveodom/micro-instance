import pandas as pd
from .Builder import Builder
from .CSV import CSV

quote_type = 'daily'
data_directory = 'data/features'
number_of_quotes_per_block = 2

def run(ticker, size):
  filename = "{0}-{1}-{2}x{2}".format(ticker,quote_type, size)
  historical_df = CSV(data_directory).open(filename)
  builder = Builder(quote_type, ticker, size, number_of_quotes_per_block)
  built_df = builder.build()
  print(built_df.tail())
  if built_df is not None:
    if historical_df is None:
      CSV(data_directory).write_df(filename, built_df)
    else:
      merged = pd.concat([historical_df, built_df]).drop_duplicates(['mc','mcdates'], keep='last')
      CSV(data_directory).write_df(filename, merged)