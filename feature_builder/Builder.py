import pandas as pd
import numpy as np
from quotes.Dataframe import Dataframe
from .MatrixChart import MatrixChart
from .Frames import Frames
from .CSV import CSV


class Builder(object):
    def __init__(self, quote_type, ticker, size, number_of_quotes_per_block):
      self.quote_type = quote_type
      self.ticker = ticker
      self.size = size
      self.number_of_quotes_per_block = number_of_quotes_per_block
      self.matrix_charts = {}

    def fetch_quotes(self):
        self.df = Dataframe(self.quote_type, self.ticker, 108).build()

    def create_frames(self):
        frames = Frames(self.df, self.size * self.number_of_quotes_per_block)
        frames.insert_id()
        self.windows = frames.create()

    def create_matrix_charts(self):
        for window in self.windows:
            if len(window) == self.size * self.number_of_quotes_per_block:
              closes = window['close'].values
              start_date = window.iloc[0,:].date
              end_date = window.iloc[-1,:].date
              key = "{0}:{1}".format(start_date, end_date)
              chart = MatrixChart(self.size).build(closes)
              
              target_index = self.df[self.df.date == end_date].index
              self.df.loc[target_index, 'mcdates'] = key
              self.df.loc[target_index, 'mc'] = chart
              
    def create_new_columns(self):
      self.df = self.df.assign(
            mc = np.zeros(len(self.df)),
            mcdates = np.zeros(len(self.df))
          )
    
    def remove_unused_columns(self):
      self.df.drop(['close', 'id', 'open', 'C/O'], axis=1,inplace=True)

    def build(self):
        self.fetch_quotes()
        if self.df is not None:
          self.create_new_columns()
          self.create_frames()
          self.create_matrix_charts()
          self.remove_unused_columns()
          return self.df
        else:
          return None