
class Frames(object):
    def __init__(self, df, frame_size):
        self.frame_size = frame_size
        self.windows = []
        self.df = df

    def insert_id(self):
        self.df.insert(0, 'id', range(0, len(self.df)))

    def create_frame(self, i):
        max = self.df['id'] < i
        min = self.df['id'] >= i - self.frame_size
        window = self.df[min & max].copy()
        self.windows.append(window)

    def create(self):
        for date, row in self.df.iterrows():
            self.create_frame(row['id'])
        return self.windows

    