import numpy as np

class MatrixChart(object):
    def __init__(self, size):
        self.size = size
        self.averages_per_row = []
        #self.output = np.zeros((size,size),dtype=int)
        self.output = np.empty((size,size), dtype='string_')
        self.output[:] = 'o'

    def reshape(self, data):
        self.ary = np.array(data).reshape(self.size,-1)

    def average_each_row(self):
        self.averages_per_row = self.ary.mean(1)

    def find_nearest(self, array, value):
        return np.abs(array-value).argmin()
    
    def get_cell_to_shade_for_rows(self):
        min = self.averages_per_row.min()
        max = self.averages_per_row.max()
        number_of_blocks = self.size - 0.1
        pct_ranges = np.arange(0,100, (1/(number_of_blocks))*100)
        percentiles = [np.percentile([min, max], pct) for pct in pct_ranges ]
        return [self.find_nearest(percentiles, num) for num in self.averages_per_row]

    def shade_cell_in_each_row(self):
        cells_to_shade_for_each_row = self.get_cell_to_shade_for_rows()
        self.output[np.arange(len(cells_to_shade_for_each_row)), cells_to_shade_for_each_row] = 'l'

    def convert_to_strings(self):
        ary = []
        for row in self.output:
            chart_str = "".join(x.decode("utf-8") for x in row)
            ary.append(chart_str)
        return "-".join(ary)

    def build(self, list_of_quotes):
        self.reshape(list_of_quotes)
        self.average_each_row()
        self.shade_cell_in_each_row()
        return self.convert_to_strings()
        


