import csv
import os
import pandas as pd

class CSV(object):
    def __init__(self, directory = "data/features", shouldCompress = False):
        self.directory = directory
        self.shouldCompress = shouldCompress
    
    def open(self, filename):
        key = "{0}/{1}.csv".format(self.directory, filename)
        if os.path.isfile(key):
          return pd.read_csv(key)
        else:
          return None

    def write_df(self, filename, df):
        key = "{0}/{1}.csv".format(self.directory, filename)
        if self.shouldCompress:
          df.to_csv(key, compression='gzip', columns=['date', 'mc', 'mcdates'])
        else:
          df.to_csv(key, columns=['date', 'mc', 'mcdates'])
        
