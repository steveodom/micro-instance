class QueryUtil(object):
    
    @staticmethod
    def projection_builder(additional_string):
      return 'date_human, ticker, {}'.format(additional_string)

