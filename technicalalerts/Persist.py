import boto3
from technical.Persist import Persist

class PersistMatches(object):
    def __init__(self, region_name="us-east-1", table_name="technical-indicator-counts"):
        self.conn = boto3.resource('dynamodb', region_name=region_name)
        if table_name:
          self.table = self.conn.Table(table_name)
        
        self.batch_items = {}
    
    def add_to_batch(self, key, data):
        data = Persist.sanitize_all(data)
        #data['id'] = key
        self.batch_items[key] = data
          
    def save_as_batch(self):
        """
        Batch write items to given table name
        """
        print('saveing_batch')
        with self.table.batch_writer() as batch:
            for id in self.batch_items:
                item = self.batch_items[id]
                batch.put_item(Item=item)
        return True