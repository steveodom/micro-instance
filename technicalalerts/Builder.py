from technicalcounter.Query import Query
from technicalcounter.QueryUtil import QueryUtil
from .QueryUtil import QueryUtil as Util
from technicalcounter.Change import Change
from technicalcounter.Macd import Macd
from technicalcounter.Patterns import Patterns
from technicalcounter.Bollinger import Bollinger
from technicalcounter.MovingAverage import MovingAverage

from .Persist import PersistMatches

class Builder(object):
    def __init__(self, quote_type = 'daily', start_time = 0, end_time = 0):
        self.quote_type = quote_type
        self.query_table = Query('us-east-1', 'TechnicalHistoryTable')
        self.persist_table = PersistMatches('us-east-1', 'TechnicalIndicatorsInstancesTable')
        self.period_query = (QueryUtil.eq('period', quote_type) & 
          QueryUtil.gt('date', start_time) &
          QueryUtil.lt('date', end_time))
        self.count_log = {}

    def save_individuals(self, queryables):
        for query in queryables:
            q = self.period_query & query['d']
            projection_expression = Util.projection_builder(query['id'])
            print(projection_expression, 'pe')
            matches = self.query_table.paginated_scan(q, projection_expression)
            print(matches, 'matches')
            self.persist_table.add_to_batch(query['id'], matches)
    
    def batch_two_queries(self, name, q1, q2, id):
        query = self.period_query & q1['d'] & q2['d']
        projection_expression = Util.projection_builder("{}, {}".format(q1['id'], q2['id']))
        print(projection_expression, 'pe2')
        matches = self.query_table.paginated_scan(query, projection_expression)
        print(matches, 'matches2')
        self.count_log[id] = len(matches)
        self.persist_table.add_to_batch(id, matches)    

    def save_with_other(self, queryables, other_queryables):
        for q1 in queryables:
            for q2 in other_queryables:
                id = "{}-{}".format(q2['id'], q1['id'])
                opp_id = "{}-{}".format(q1['id'], q2['id'])
                if q1['s'] is not q2['s'] and opp_id not in self.count_log:
                    self.batch_two_queries('all', q1, q2, id) 

    def query(self):
        
        macd = Macd()
        self.save_individuals(macd.queryables())
        self.save_with_other(macd.queryables(), macd.queryables())
        print('finished macd')
        
        patt = Patterns()
        self.save_individuals(patt.queryables())
        self.save_with_other(patt.queryables(), patt.queryables())
        self.save_with_other(patt.queryables(), macd.queryables())
        print('finished patterns')
                    
        boll = Bollinger()
        self.save_individuals(boll.queryables())
        self.save_with_other(boll.queryables(), boll.queryables())
        self.save_with_other(boll.queryables(), patt.queryables())
        self.save_with_other(boll.queryables(), macd.queryables())
        print('finished bollinger')

        ma = MovingAverage()
        self.save_individuals(ma.queryables())
        self.save_with_other(ma.queryables(), ma.queryables())
        self.save_with_other(ma.queryables(), boll.queryables())
        self.save_with_other(ma.queryables(), patt.queryables())
        self.save_with_other(ma.queryables(), macd.queryables())

        print('finished moving average')
        return self.persist_table.batch_items

    def query_and_save(self):
        self.query()
        #self.persist_table.save_as_batch()
        return self.persist_table.batch_items